.. _Merging_Repositories:

Merging Repositories
====================

Question: Can we merge a repository into another without losing history?
For merging two existing repositories a lot can be found on the internet. Eventually I merged two found methods into
one method. The second part of the first method did not result in the wanted behavior. For that the second method was
used.

The found methods:

    * `method 1 <https://www.atlassian.com/git/tutorials/git-move-repository>`_
    * `method 2 <https://medium.com/@ayushya/move-directory-from-one-repository-to-another-preserving-git-history-d210fa049d4b>`_

.. tip::

    Within the following steps repository A will be merged into repository B. It is wise to create a temporary directory
    in which both repositories will be cloned, so when a certain step does not have the wanted outcome it will be fairly easy
    to start all over again.

.. note::

   Most of the steps below are shown from a script used to execute the merge between two repositories, see :download:`merge_git_repo_a_to repo_b <./shell-scripts/merge_git_repo_a_to_repo_b>`.

Steps
-----

Create temporary directory:

.. code-block:: bash

   mkdir $HOME/tmp/merge_git_repos
   cd $HOME/tmp/merge_git_repos

Clone repository A into a temporary name tmp_repo_a:

.. code-block:: bash

    git clone <url of repo a> tmp_repo_a


Clone repository B into a temporary name tmp_repo_b:

.. code-block:: bash

   git clone <url of repo b> tmp_repo_b


Execute script for merging repo a into repo b:

.. code-block:: bash

   merge_git_repo_a_to_repo_b

.. note::

   The script can also be used with the locations of the two repositories (e.g. merge_git_repo_a_to_repo_b <location repo a> <location repo b>. If the location are omitted the defaults tmp_repo_a and tmp_repo_b are used.

Executed steps from the shell script:

Checkout all the branches you want to copy from repository A:

.. literalinclude:: ./shell-scripts/merge_git_repo_a_to_repo_b
   :language: bash
   :dedent: 3
   :lines: 54-59

Fetch all the tags from repository A:

.. literalinclude:: ./shell-scripts/merge_git_repo_a_to_repo_b
   :language: bash
   :dedent: 3
   :lines: 60

Check if you have all the tags and branches:

.. literalinclude:: ./shell-scripts/merge_git_repo_a_to_repo_b
   :language: bash
   :dedent: 3
   :lines: 62-63

Clear the link to the remote of repository A:

.. literalinclude:: ./shell-scripts/merge_git_repo_a_to_repo_b
   :language: bash
   :dedent: 3
   :lines: 65

Do some filtering for rewriting the history:

.. literalinclude:: ./shell-scripts/merge_git_repo_a_to_repo_b
   :language: bash
   :dedent: 3
   :lines: 78-92

.. tip::

    The index-filter takes less time in comparison to the tree-filter and can be used for removing files/directories. Also sometimes removing files/directories using the tree-filter does not always rewrite history.

    For changing the directory structure the following commands can be used within a tree-filter

    > mkdir -p <new directory>;git mv -k <file to move> <new directory>

    For removing a file/directory the following command can be used within a index-filter

    > git rm -r --cached --ignore-unmatch <file or directory>

.. note::

    The order of the actions within the commands or executed filtering is very important. It is best to first move sources and finally removing files/directories, since the second filtering must use option -f (force) to be executed.

Removing garbage from filtering:

.. literalinclude:: ./shell-scripts/merge_git_repo_a_to_repo_b
   :language: bash
   :dedent: 3
   :lines: 103-106

Perfrom any necessary changes and commit these:

.. code-block:: bash

   git status
   git add .
   git commit

Create a new branch in repository B:

.. literalinclude:: ./shell-scripts/merge_git_repo_a_to_repo_b
   :language: bash
   :dedent: 3
   :lines: 117-119

Create a remote connection to repository A as a branch in repository B:

.. literalinclude:: ./shell-scripts/merge_git_repo_a_to_repo_b
   :language: bash
   :dedent: 3
   :lines: 120

.. note::

   repo-a can be anything - it's just a random name

Pull files and history from branches into repository B:

.. literalinclude:: ./shell-scripts/merge_git_repo_a_to_repo_b
   :language: bash
   :dedent: 3
   :lines: 121

Remove the remote connection to repository A:

.. literalinclude:: ./shell-scripts/merge_git_repo_a_to_repo_b
   :language: bash
   :dedent: 3
   :lines: 122

Finally, push the changes:

.. code-block:: bash

   git push origin <new branch name>

