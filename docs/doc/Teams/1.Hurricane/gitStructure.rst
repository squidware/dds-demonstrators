

Git workflow DDS-Demonstrators
==============================

There are several workflows for using git. We started with a centralized workflow. However a bottleneck was encountered
in the time to be spent on the pull-requests towards the central repository. Any subsequent changes to the 'local' master
influenced the outstanding pull-requests. This needed to be addressed, by finding a different workflow to be used where
all the parties could continue.

The following workflows have been examined, `link <https://git-scm.com/book/en/v2/Distributed-Git-Distributed-Workflows>`_:
   * Integration-manager workflow
   * Dictator and lieutenant workflow

The result is a variant of both.

.. uml::

   digraph git {
      node [style="rounded,filled"]
      rankdir=BT

      m [label="main\nrepository", shape=box, fillcolor=orange]
      gk [label="gatekeeper\nrepository", shape=box, fillcolor=yellow]
      d1 [label="developer 1\nrepository", shape=box, fillcolor=DodgerBlue]
      dn [label="developer n\nrepository", shape=box, fillcolor=DodgerBlue]

      gk -> m [label="fork"]
      d1 -> gk [label="fork"]
      dn -> gk [label="fork"]
   }

The gatekeeper forks the central repository and the developers fork the gatekeeper repository. For the gatekeeper and
developers repository the **dev** branch is used for pushing/pulling changes using pull-requests. Only the gatekeeper
will address the **master** branch on the central repository by means of pull-requests.

So, for all changes to be merged (independent of the repository) a pull-request is needed to uphold the needed
quality for documentation and source.
Because all developers fork the gatekeeper, they can benefit from each other and there would be no need to interchange
documentation or source between their repositories and avoids any merge conflicts.

Some workflows:

.. uml::

   start
   :receive pull request on dev from developer;
   if (approved) then (yes)
      :merge to dev;
      :pull request to __own__ __master__;
      if (approved) then (yes)
         :merge to master;
         :pull request to __main__ __master__;
      else (no)
      endif
   else (no)
   endif
   stop

.. uml::

   start
   :create feature;
   if (done) then (yes)
      :pull request to __own__ __dev__;
      if (approved) then (yes)
         :merge to own dev;
         :pull request to __gatekeeper__ __dev__;
      else (no)
      endif
   else (no)
   endif
   stop
