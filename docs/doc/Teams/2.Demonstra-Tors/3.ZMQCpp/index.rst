.. _zmq_cpp:

ZMQ C++
------------------

:authors: Sam Laan
:date: March 2020

Description
"""""""""""
ZeroMQ (also known as ØMQ, 0MQ, or zmq) is a message-oriented middleware. 
It is used in various environments for example in embedded systems, 
aerospace and financial services.
ZeroMQ provides a library, not a messaging server.
It gives sockets that carry atomic messages across various transports 
like in-process, inter-process, TCP, and multicast.
Each socket can handle communication with multiple peers.
ZeroMQ allows these sockets to be connected through different patterns.
The protocol uses ZMTP(ZeroMQ Message Transport Protocol) 
on the wire which makes use of message batching, 
asynchronous communication and supports zero-copy.
ZeroMQ core is written in C/C++ and has bindings for most modern programming 
languages and operating systems.

ZMQ install
""""""""""""

To use ZeroMQ the C library has to be installed this can be done like so:

.. code-block:: C++
   
   git clone https://github.com/zeromq/libzmq
   cd libzmq
   mkdir build
   cd build
   sudo cmake ..
   sudo make install

After this the C++ lib can be installed.
The cppzmq was chosen because it's the most lightweight, 
the most used and the best mantained C++ lib.

.. code-block:: C++
   
   git clone https://github.com/zeromq/cppzmq
   cd cppzmq
   mkdir build
   cd build
   sudo cmake ..
   sudo make install

ZMQ solutions
""""""""""""""
This list contains the solutions regarding ZMQ and C++. 

*  :ref:`zmq_cpp_matrixboard`

Links
"""""
* List of bindings: http://wiki.zeromq.org/bindings:_start
