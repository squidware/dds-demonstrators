.. _clean_install_with_internet:

Clean Install with internet
---------------------------

:authors: Joost Baars
:date: Mar 2020

This page describes how the general DDS necessities can be installed (Cyclone 
DDS library and the requirements for the documentation). This tutorial was 
tested from a clean Raspbian Buster Lite installation (version 13-03-2020).

This tutorial is specifically for Linux systems based on Debian. 
Therefore, this tutorial also works for Ubuntu systems.

This tutorial also specifies what commands need root access. These commands 
use ``sudo`` in front of the actual command that is executed. 

Start with the following commands to make sure the repositories are up to date:

  .. code-block:: bash

   sudo apt-get update && sudo apt-get upgrade
  
Cyclone DDS install
"""""""""""""""""""
For this library, you have to install the packages using this command:

  .. code-block:: bash

   sudo apt-get install cmake git openjdk-11-jdk-headless maven openssl

If you don't have a C++ compiler, it can be installed with the following command:

  .. code-block:: bash

   sudo apt-get install gcc

Install Cyclone DDS:

  .. code-block:: bash

   git clone https://github.com/eclipse-cyclonedds/cyclonedds.git
   cd cyclonedds
   mkdir build
   cd build
   cmake ..
   make
   sudo make install

If nothing generated errors, you succesfully installed Cyclone DDS.

.. note:: Cyclone DDS version used in this tutorial

   In general you can just use the newest version of Cyclone DDS. 
   But if the newest version does not work for any reason, this tutorial used 
   the last commit: 269f18e.

   This commit can be used by using the command:

      .. code-block:: bash
 
       git checkout 269f18e

Documentation
"""""""""""""

The documentation needs the following packages to be installed:

  .. code-block:: bash

   sudo apt-get install python3 plantuml python3-pip python3-sphinx
   pip3 install sphinxcontrib.napoleon sphinxcontrib-plantuml sphinxcontrib.needs

Now the repository of the DDS-Demonstrators can be downloaded and the 
documentation can be build. For the dependencies of individual software programs 
on this repository, and how to run it. You should check the documentation page 
containing that application.

  .. code-block:: bash

   git clone https://bitbucket.org/HighTech-nl/dds-demonstrators.git
   cd dds-demonstrators/docs
   make

The documentation is stored in the ``__result`` directory. Now you should open 
``__result/html/index.html`` with your browser for the documentation.

If you use a windows linux subsystem (WSL), then you can use the following 
command for viewing the documentation in your browser. Otherwise, you have to replace 
``x-www-browser`` with the browser you have installed.

  .. code-block:: bash

   x-www-browser __result/html/index.html
