.. _mb_performance_application_cpp:

Matrix board performance measurements
-------------------------------------

:authors: Joost Baars
:date: June 2020

Description
"""""""""""
The matrix board performance measurements execute performance measurements 
on the matrix board demonstrator (see: :ref:`demonstrators_matrixboard`).

In these measurements, different Quality of Service (QoS) policies are compared 
with each other. The Matrixboard demonstrator mainly shows the time it takes for 
a new device to connect or disconnect using DDS. 

The selected QoS policies are the durability, liveliness, lifespan, reliability 
and the deadline QoS policies (see :ref:`quick_performance_tests`).

A short description of how the connect and disconnect durations are measured 
can be seen below (see chapter `Connect duration`_ & `Disconnect duration`_)

An application has been designed for measuring the connect and disconnect duration 
automatically. This application and the results of the measurements are explained 
on the pages beneath. The system configuration shows how to configure the 
dependencies of the performance measurements. The performance application page 
describes the application in detail: how the application can be executed as well 
as how the application works from a technical point of view. The performance 
measurement results show the results of the performance measurements of the 
selected QoS policies. 

.. toctree::
    :titlesonly:
    :maxdepth: 0
    :glob:

    system_configuration
    performance_application
    performance_measurements
    measurement_analysis

Connect duration
""""""""""""""""
The connect duration is measured between registering to a topic and being notified 
that a device is connected to the topic. This principle is displayed below for 
``Application1``. In this figure, the process that determines the duration between 
registering and other devices being notified is displayed. The duration between 
the start of the "registers to topic" arrow and the end of the "notified" arrow is 
the connect duration. 

.. uml::

    Application1 -> Topic : Registers to topic
    Topic -> "Application n" : Notified that a new device has registered to the topic

Disconnect duration
"""""""""""""""""""
The disconnect duration is measured between disconnecting from a topic and being 
notified that a device has disconnected from the topic. This principle is displayed 
below for ``Application1``. In this figure, the process that determines the duration 
between disconnecting and other devices being notified is displayed. The duration 
between the start of the "Disconnects from topic" arrow and the end of the "notified" 
arrow is the disconnect duration.

.. uml::

    Application1 -x Topic : Disconnects from topic
    Topic -> "Application n" : Notified that a device has disconnected from the topic

Helper pages
"""""""""""""
The following pages are written for explaining some topics. This information does not 
contain information needed for the performance measurements, but contains information 
that could be useful for debugging or learning more about the tools.

.. toctree::
    :titlesonly:
    :maxdepth: 0
    :glob:

    systemctl_basics