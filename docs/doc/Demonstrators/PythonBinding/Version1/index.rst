Version1
--------
.. warning::
    This version is deprecated. It describes an older version of the library, which is no longer in use. This page is only kept for development purposes, and will be removed shortly.

.. note::
    The code for this version is located in /poc1


Description
"""""""""""
Demonstrator for the new Python binding. Main features are (once finsihed):
- Full feature set (all functionalities of the underlying DDS library available)
- Able to handle idl files properly (the atolab implementation converts everything to a string, and disregards idl files).
- Pythonic implementation, to ensure the easy of use for developers.

Currently there is a minimal version available. It is possible to send and receive basic messages, using the json method. IDL support
will be added in the near future.

Assumptions
"""""""""""
The following library files need to be in the stated directory:

- libddsc.so at "/usr/local/lib"
- libddstubs.so at "/usr/local/lib"

The latter is the one coming from the atolab implementation, and is only needed temporarily. 
It contains the topic descriptor for the json topic. Once IDL support is added it will be removed.

Design
""""""
The UML of the implementation:

.. uml::

   @startuml
   class Participant {
       ID: int
   }
   class Topic {
       name: str
       participant: Participant
       ID: int
   }
   class Writer {
       participant: Participant
       topic: Topic
       ID: int
       --
       __init__(Participant, Topic)
       __call__(message)
   }
   class Listener {
       ID: int
   }
   class Reader {
       participant: Participant
       topic: Topic
       listener: Listener
       ID: int
       buffer: [void]
       ivec: [SampleInfo]
       size: int
       ---
       __call__(as_type:type=None)
   }
   Topic o-- Participant
   Writer o-- Participant
   Writer o-- Topic
   Reader o-- Participant
   Reader o-- Topic
   Reader o-- Listener
   @enduml


Examples
""""""""
The files `sender.py` and `receiver.py` demonstrate the usage of the library. Sender publishes a simple
message to the topic "testtopic" every 2 seconds, and logs it to the console.

Receiver reads the same topic, once every 3 seconds, and logs the list of read messages to the console.
(It always reads a list of messages, so reading more than one at a time is not a problem.)

The files can be called from the `Examples` folder as follows:

.. code-block:: python

   python3 sender.py

and

.. code-block:: python

   python3 receiver.py