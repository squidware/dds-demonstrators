MQTT-PingPing
-------------
* There is only 1 program: PingPong
* You need to a MQTT broker: See http://mosquitto.org
* Start that first, then run PingPong
* It will print some status EVERY few messages; seconds or seconds -- depending on system-speed
* Terminate with ^C

Note: on a RaspberryPi, when mosquitto is installed (apt-get) it will run a broker by default -- fine

