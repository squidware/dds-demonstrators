.. _mqtt_cpp_roundtrip:

MQTT Round trip in C++
----------------------

:authors: Sam Laan
:date: Apr 2020

Description
"""""""""""
The round trip C++ directory can be found in 
``src/demonstrators/ComparedToOtherProtocols/MQTT/RoundTrip/C++/``.
This directory contains a folder for the round trip implementation as well as 
the flood implementation.

This page contains information about the round trip and flood applications 
made in C++ using MQTT.

Dependencies
""""""""""""

The dependencies necessary for successful compilation of the round trip / flood 
application in C++ are the following:

 * CMake
 * C++17 compiler (GCC 7+ for example)
 * paho.mqtt.cpp library (installation: :ref:`mqtt_cpp`)
 * MQTT server compatible with MQTT 5 (mosquitto(installation: :ref:`mqtt_cpp`) for example)

Round trip
""""""""""
This chapter contains information about the round trip application. 
This application can be found in the directory: 
``src/demonstrators/ComparedToOtherProtocols/MQTT/RoundTrip/C++/RoundTrip``.

Configuring
~~~~~~~~~~~
The CMake file can be found in the source directory of the round trip.
This file can be configured to either build the implementation using polling 
for the round trip or the one using a callback.
To build the polling implementation make the following change to the Cmake file.

.. code-block:: bash

    set(PROJECT roundtrip_read)

The line to change can be found at the top of the Cmake file.
To build the callback implementation set this line to:

.. code-block:: bash

    set(PROJECT roundtrip_callback)

Building
~~~~~~~~
To build the application run the following commands in the round trip 
implementation source directory.

.. code-block:: bash

    mkdir build && cd build
    cmake ..
    make

These commands will compile the source code and generate an executable.

Execution
~~~~~~~~~
The application takes several parameters for its execution.
It can be executed using the following command in the build folder:

.. code-block:: bash

    ./RoundTrip <device ID> <number of devices> <total round trips> <QoS>

A more thorough description of the parameters can be found when executing 
the application without parameters or can be read from the source of main.cpp.
The following example starts 4 nodes for the round trip using MQTT QoS level 1. 
The slaves of the round trip are started in the background. 
Only the master is started in the foreground in this example.
Each device pings a total of 1000 times. Therefore, there are 1000 round trips. 

.. code-block:: bash

    ./RoundTrip 2 4 1000 1 & ./RoundTrip 3 4 1000 1 & ./RoundTrip 4 4 1000 1 &
    ./RoundTrip 1 4 1000 1

.. note:: Execution of the program

   The round trip is initiated by the device with <device ID> = 1. Therefore, 
   <device ID> = 1 should always be started last.
   
   The devices added with <device ID> must be an increment of the previous one. 
   This function does not dynamically search for the next device! 
   So if there are four devices, device 2 should be started first, then 3 after that 4 
   and as last device 1 should be started as 1 should always be started last.
   
   The <total round trips> parameter should be the same for each application.

Implementation
~~~~~~~~~~~~~~
Each round trip application creates an MQTT client.
The client publishes to the topic of the ID above it. 
So the application with ID = 1 publishes to the application with ID = 2 and so on.
The client reads from its topic. These topics have the name "roundtrip" 
with the ID behind it. 
So the topic with ID = 1 is "roundtrip1".

The application with ID = 1 initiates the round trip.
Therefore, it starts with publishing to topic "roundtrip2". 
The application with ID = 2 then receives the message of the application with ID = 1 
and sends a message to the next application.
The last application always publishes to the first making the round trip complete.

Read
****
The read implementation continuously executes the ``try_consume_message`` function to get 
the latest message of its own topic. 
``try_consume_message`` tries to read the next message from the queue without blocking. 
When a message is received, the application uses ``publish`` to publish to the next application.

Callback
********
The callback implementation uses a callback function for receiving the latest message. 
This callback sets a message flag which will trigger the publishing of a 
message to the next application.

Flood
"""""
This chapter contains information about the flood application. 
This application can be found in the directory: 
``src/demonstrators/ComparedToOtherProtocols/MQTT/RoundTrip/C++/Flood``.

Building
~~~~~~~~
First, go to the flood C++ directory (see `Flood`_).

Execute the following commands:

.. code-block:: bash

    mkdir build && cd build
    cmake ..
    make
    
These commands will compile the source code and generate an executable.

Execution
~~~~~~~~~
The application takes several parameters for its execution.
It can be executed using the following command in the build folder:

.. code-block:: bash

    ./Flood <device ID> <number of devices> <total messages> <QoS>

A more thorough description of the parameters can be found when executing the application 
without parameters or can be read from the source of main.cpp.
The following example starts 4 nodes for the flood using MQTT QoS level 1. 
The slaves of the round trip are started in the background. 
There are a total of 1000 messages sent by the master.
For a flood to be finished, these messages should all be correctly received by the master.

.. code-block:: bash

    ./Flood 2 4 1000 1 & ./Flood 3 4 1000 1 & ./Flood 4 4 1000 1 &
    ./Flood 1 4 1000 1

.. note:: Execution of the program

   The flood is initiated by the device with <device ID> = 1. Therefore, 
   <device ID> = 1 should always be started last.
   
   The devices added with <device ID> must be an increment of the previous one. 
   This function does not dynamically search for the next device! 
   So if there are four devices, device 2 should be started first, then 3, after that 4 
   and as last device 1 should be started as 1 should always be started last.
   
   The <total messages> parameter should be the same for each application.

Implementation
~~~~~~~~~~~~~~
Each flood application creates an MQTT client.
The client publishes to the topic of the ID above it. 
So the application with ID = 1 publishes to the application with ID = 2 and so on.
The client reads from its topic. These topics have the name "flood" 
with the ID behind it. 
So the topic with ID = 1 is "flood1".

The application with ID = 1 initiates the flood. 
It does this by publishing to topic "flood2". 
The application with ID = 2 then receives the messages of the application with ID = 1 
and sends a message to the next application for each message received.

The slaves (ID not equal to 1) just send a message to the next device in the flood 
loop as soon as they receive a message.
The master (ID = 1) sends messages as fast as possible to the next device in the 
flood loop.

If the master sends a message, a value is incremented to keep track 
of the number of messages sent. 
The master keeps sending messages until this value is equal to the ``<total messages>`` 
parameter that the user inserted when executing the application.
If the master receives a message, another value is incremented. 
In the end, this value should also be the same as the ``<total messages>`` 
parameter that the user inserted when executing the application.

Slave
*****
The slave implementation uses a callback which sets a message flag on receiving a message. 
these messages are received on its topic. 
When a message is received, the application uses ``publish()`` for publishing 
a message to the next application.

Master
******
The master implementation uses a callback which sets a message flag on receiving a message. 
these messages are received on its topic. 
Afterwards, it writes a new message to the next device in the flood loop. 

Therefore, in the beginning, the master only writes to the next devices. 
Afterwards, the loop will eventually be in a stable condition where the master reads 
a message first and writes a new message afterwards.
In the end, the master won't send messages anymore, and will only read the last messages 
that exist in the flood loop.

