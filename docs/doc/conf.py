# Copyright (C) ALbert Mietus, Sogeti-HT  2019,
# -*- coding: utf-8 -*-

# read STD config ...
#==========================================
import sys; sys.path.append('../_std_settings/conf')
from std_conf import *


# General information about the project.
#======================================
project = 'DDS-Demonstrators'
version = "TechPush"                                                    # |version|
copyright = "Sogeti-HT-NL, 2019"

from datetime import datetime
build = datetime.now().strftime("%Y.%m.%d")

release = "%s-%s" % (version, build)                                    # |release|

# Overrule std_conf, where needed
#================================

release = version
html_title = project + " | " + release # DEFAULT: '<project> v<revision> documentation' -- Strip "documentation"

# Needs configuration
#====================
from docutils.parsers.rst import directives

needs_statuses = False

needs_extra_options = {
 "requirement": directives.unchanged
}
