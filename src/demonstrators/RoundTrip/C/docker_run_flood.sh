#!/bin/bash

echo "Deleting old log file."
rm $PWD/flood.log

echo "Running Flood Demonstrator...press ^C to exit"
./Hub_flood topic_2 topic_3 &
./Hub_flood topic_3 topic_4 &
./Hub_flood topic_4 topic_1 &
./Master_flood topic_1 topic_2 > flood.log
