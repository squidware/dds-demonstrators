#ifndef CONFIGURATIONS_H
#define CONFIGURATIONS_H

#include "dds/dds.h"

#include <iostream>

/**
 * @brief Contains a lot of Quality of Service policies for configuring DDS within Cyclone DDS
 *
 */
class Configurations {
  public:
    static void deadlineCallback(dds_entity_t reader, const dds_requested_deadline_missed_status_t status, void *arg);
    static void deadline(dds_qos_t *qos, dds_listener_t *listener, const dds_time_t maxDeadline);
    static void destination_order(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration = 0);
    static void durability(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration = 0);
    static void lifespan(dds_qos_t *qos, dds_listener_t *listener, const dds_time_t maxLifespan = 0);
    static void livelinessCallback(dds_entity_t reader, const dds_liveliness_changed_status_t status, void *arg);
    static void liveliness(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration = 0);
    static void ownership(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration = 0);
    static void reader_data_lifecycle(dds_qos_t *qos, dds_listener_t *listener, const dds_time_t configuration = 0);
    static void durability_service(dds_qos_t *qos, dds_listener_t *listener, const dds_time_t configuration = 0);
    static void history(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration = 0);
    static void reliability(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration = 0);
    static void latency_budget(dds_qos_t *qos, dds_listener_t *listener, const dds_time_t configuration = 0);
    static void presentation(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration = 0);
    static void resource_limits(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration = 0);
    static void time_based_filter(dds_qos_t *qos, dds_listener_t *listener, const dds_time_t configuration = 0);
    static void transport_priority(dds_qos_t *qos, dds_listener_t *listener, const int64_t priority = 0);
    static void writer_data_lifecycle(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration = 0);
    static void partition(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration = 0);
};

#endif