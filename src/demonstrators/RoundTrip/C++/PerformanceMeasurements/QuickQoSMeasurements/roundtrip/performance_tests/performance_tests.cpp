#include "performance_tests.hpp"

#include "configurations/configurations.h"

#include <chrono>
#include <iostream>
#include <thread>

PerformanceTests::PerformanceTests(const unsigned int id, const unsigned int totalDevices,
                                   const unsigned int roundtrips, const std::string filename)
    : _id{id}, _totalRoundtrips{roundtrips}, _totalDevices{totalDevices}, _measurementsFile{filename} {}

/**
 * @brief start starts a lot of different tests with different configurations
 *
 */
void PerformanceTests::start() { globalTests(); }

/**
 * @brief globalTests configures the default QoS policies for the qosPolicyTests()
 *
 */
void PerformanceTests::globalTests() {
    QoSManager::QoSFunction configDefault, configDefault2;

    _qosConfig.resetDefaultQoS();
    qosPolicyTests();

    configDefault(Configurations::reliability, DDS_RELIABILITY_RELIABLE);
    _qosConfig.configureDefaultQoS(configDefault);
    qosPolicyTests();

    _qosConfig.resetDefaultQoS();

    configDefault(Configurations::reliability, DDS_RELIABILITY_BEST_EFFORT);
    _qosConfig.configureDefaultQoS(configDefault);
    qosPolicyTests();

    _qosConfig.resetDefaultQoS();

    configDefault(Configurations::reliability, DDS_RELIABILITY_BEST_EFFORT);
    configDefault2(Configurations::durability, DDS_DURABILITY_TRANSIENT_LOCAL);
    _qosConfig.configureDefaultQoS(configDefault, configDefault2);
    qosPolicyTests();

    _qosConfig.resetDefaultQoS();

    configDefault(Configurations::reliability, DDS_RELIABILITY_BEST_EFFORT);
    configDefault2(Configurations::durability, DDS_DURABILITY_VOLATILE);
    _qosConfig.configureDefaultQoS(configDefault, configDefault2);
    qosPolicyTests();

    _qosConfig.resetDefaultQoS();

    configDefault(Configurations::reliability, DDS_RELIABILITY_RELIABLE);
    configDefault2(Configurations::durability, DDS_DURABILITY_TRANSIENT_LOCAL);
    _qosConfig.configureDefaultQoS(configDefault, configDefault2);
    qosPolicyTests();

    _qosConfig.resetDefaultQoS();

    configDefault(Configurations::reliability, DDS_RELIABILITY_RELIABLE);
    configDefault2(Configurations::durability, DDS_DURABILITY_VOLATILE);
    _qosConfig.configureDefaultQoS(configDefault, configDefault2);
    qosPolicyTests();

    // End with the test at the start. Because it seems like the first measurements are the slowest
    _qosConfig.resetDefaultQoS();
    qosPolicyTests();
}

/**
 * @brief qosPolicyTests contains different tests that will be executed
 * This function contains all configurations with different parameters
 * This function can be executed with various default parameters
 *
 */
void PerformanceTests::qosPolicyTests() {
    QoSManager::QoSFunction config, config2;

    _qosConfig.configure();
    executeTest("default", &_qosConfig);

    config(Configurations::deadline, DDS_SECS(10));
    _qosConfig.configure(config);
    executeTest("deadline 10s", &_qosConfig);

    config(Configurations::deadline, DDS_MSECS(10));
    _qosConfig.configure(config);
    executeTest("deadline 10ms", &_qosConfig);

    config(Configurations::destination_order, DDS_DESTINATIONORDER_BY_RECEPTION_TIMESTAMP);
    _qosConfig.configure(config);
    executeTest("destination order reception", &_qosConfig);

    config(Configurations::destination_order, DDS_DESTINATIONORDER_BY_SOURCE_TIMESTAMP);
    _qosConfig.configure(config);
    executeTest("destination order source", &_qosConfig);

    config(Configurations::durability, DDS_DURABILITY_PERSISTENT);
    _qosConfig.configure(config);
    executeTest("durability persistent", &_qosConfig);

    config(Configurations::durability, DDS_DURABILITY_TRANSIENT);
    _qosConfig.configure(config);
    executeTest("durability transient", &_qosConfig);

    config2(Configurations::durability_service, DDS_SECS(1));
    _qosConfig.configure(config, config2);
    executeTest("durability transient with durability service 1s", &_qosConfig);

    config2(Configurations::durability_service, DDS_MSECS(10));
    _qosConfig.configure(config, config2);
    executeTest("durability transient with durability service 10ms", &_qosConfig);

    config(Configurations::durability, DDS_DURABILITY_TRANSIENT_LOCAL);
    _qosConfig.configure(config);
    executeTest("durability transient local", &_qosConfig);

    config(Configurations::durability, DDS_DURABILITY_VOLATILE);
    _qosConfig.configure(config);
    executeTest("durability volatile", &_qosConfig);

    config(Configurations::lifespan, DDS_MSECS(10));
    _qosConfig.configure(config);
    executeTest("lifespan 10ms", &_qosConfig);

    config(Configurations::lifespan, DDS_MSECS(100));
    _qosConfig.configure(config);
    executeTest("lifespan 100ms", &_qosConfig);

    config(Configurations::liveliness, DDS_LIVELINESS_AUTOMATIC);
    _qosConfig.configure(config);
    executeTest("liveliness automatic", &_qosConfig);

    config(Configurations::liveliness, DDS_LIVELINESS_MANUAL_BY_PARTICIPANT);
    _qosConfig.configure(config);
    executeTest("liveliness manual by participant", &_qosConfig);

    config(Configurations::liveliness, DDS_LIVELINESS_MANUAL_BY_TOPIC);
    _qosConfig.configure(config);
    executeTest("liveliness manual by topic", &_qosConfig);

    config(Configurations::ownership, DDS_OWNERSHIP_SHARED);
    _qosConfig.configure(config);
    executeTest("ownership shared", &_qosConfig);

    config(Configurations::ownership, DDS_OWNERSHIP_EXCLUSIVE);
    _qosConfig.configure(config);
    executeTest("ownership exclusive", &_qosConfig);

    config(Configurations::reader_data_lifecycle, DDS_SECS(1));
    _qosConfig.configure(config);
    executeTest("data lifecycle qos id 1s", &_qosConfig);

    config(Configurations::reader_data_lifecycle, DDS_MSECS(10));
    _qosConfig.configure(config);
    executeTest("data lifecycle qos id 10ms", &_qosConfig);

    config(Configurations::reliability, DDS_RELIABILITY_BEST_EFFORT);
    _qosConfig.configure(config);
    executeTest("reliability best effort", &_qosConfig);

    config(Configurations::reliability, DDS_RELIABILITY_RELIABLE);
    _qosConfig.configure(config);
    executeTest("reliability reliable", &_qosConfig);

    config(Configurations::history, DDS_HISTORY_KEEP_ALL);
    _qosConfig.configure(config);
    executeTest("history all", &_qosConfig);

    config(Configurations::history, DDS_HISTORY_KEEP_LAST);
    _qosConfig.configure(config);
    executeTest("history keep last", &_qosConfig);

    config(Configurations::latency_budget, DDS_SECS(1));
    _qosConfig.configure(config);
    executeTest("latency budget 1000ms", &_qosConfig);

    config(Configurations::latency_budget, DDS_MSECS(10));
    _qosConfig.configure(config);
    executeTest("latency budget 10ms", &_qosConfig);

    config(Configurations::presentation, DDS_PRESENTATION_TOPIC);
    _qosConfig.configure(config);
    executeTest("presentation topic", &_qosConfig);

    config(Configurations::presentation, DDS_PRESENTATION_GROUP);
    _qosConfig.configure(config);
    executeTest("presentation group", &_qosConfig);

    config(Configurations::presentation, DDS_PRESENTATION_INSTANCE);
    _qosConfig.configure(config);
    executeTest("presentation instance", &_qosConfig);

    config(Configurations::resource_limits, 500);
    _qosConfig.configure(config);
    executeTest("resource limits", &_qosConfig);

    config(Configurations::time_based_filter, DDS_SECS(1));
    _qosConfig.configure(config);
    executeTest("time based filter 1s", &_qosConfig);

    config(Configurations::time_based_filter, DDS_MSECS(10));
    _qosConfig.configure(config);
    executeTest("time based filter 10ms", &_qosConfig);

    config(Configurations::transport_priority, 1);
    _qosConfig.configure(config);
    executeTest("transport priority of 1", &_qosConfig);

    config(Configurations::transport_priority, 100);
    _qosConfig.configure(config);
    executeTest("transport priority of 100", &_qosConfig);

    config(Configurations::writer_data_lifecycle, 1);
    _qosConfig.configure(config);
    executeTest("writer data lifecycle true", &_qosConfig);

    config(Configurations::writer_data_lifecycle, 1);
    _qosConfig.configure(config);
    executeTest("writer data lifecycle false", &_qosConfig);

    config(Configurations::partition, 1);
    _qosConfig.configure(config);
    executeTest("partition", &_qosConfig);

    QoSManager::QoSFunction config3, config4, config5, config6, config7;

    config(Configurations::history, DDS_HISTORY_KEEP_LAST);
    config2(Configurations::reliability, DDS_RELIABILITY_BEST_EFFORT);
    config3(Configurations::ownership, DDS_OWNERSHIP_SHARED);
    config4(Configurations::durability, DDS_DURABILITY_TRANSIENT_LOCAL);
    config5(Configurations::presentation, DDS_PRESENTATION_INSTANCE);

    _qosConfig.configure(config, config2, config3, config4, config5);
    executeTest("high performance", &_qosConfig);

    config(Configurations::history, DDS_HISTORY_KEEP_ALL);
    config2(Configurations::reliability, DDS_RELIABILITY_RELIABLE);
    config3(Configurations::ownership, DDS_OWNERSHIP_EXCLUSIVE);
    config4(Configurations::lifespan, DDS_MSECS(10));
    config5(Configurations::deadline, DDS_MSECS(10));
    config6(Configurations::liveliness, DDS_LIVELINESS_AUTOMATIC);
    config7(Configurations::latency_budget, DDS_SECS(1));

    _qosConfig.configure(config, config2, config3, config4, config5, config6, config7);
    executeTest("high overhead", &_qosConfig);
}

/**
 * @brief executeTest executes a test
 *
 * @param testName the name of the test
 * @param qosConfig the configuration of the test
 */
void PerformanceTests::executeTest(const std::string &testName, QoSManager *qosConfig) {
    if (_id == 1) {
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
    }
    RoundTrip roundtrip(_id, _totalDevices, _totalRoundtrips);
    roundtrip.initialise(qosConfig);
    roundtrip.run();
    const auto totalRoundtripTime = roundtrip.getTime();

    if (_id != 1) {
        return;
    }
    const double timePerRoundtrip{totalRoundtripTime / static_cast<double>(_totalRoundtrips)};
    const double timeBetweenDevices{timePerRoundtrip / _totalDevices};

    _measurementsFile.write(testName, timeBetweenDevices);
    std::cout << "RoundTrip " << testName << " finished!\n"
              << "Total execution time for " << _totalRoundtrips << " roundtrips: " << totalRoundtripTime
              << " microseconds\n"
              << "Average time per round trip: " << timePerRoundtrip << " microseconds\n"
              << "Time between each device within the roundtrip: " << timeBetweenDevices << " microseconds\n";
    // std::cout << "Estimated memory usage: " << roundtrip.getMemoryUsage() << "\n"
    //         << "Estimated CPU time used: " << roundtrip.getCPUusage() << " seconds\n\n";
}