#ifndef DEFAULT_QOS_ROUNDTRIP_H
#define DEFAULT_QOS_ROUNDTRIP_H

#include "dds/ddsi/ddsi_xqos.h"
#include <dds/dds.h>

/**
 * @brief QoSManager manages the QoS objects for the performance tests
 * This class contains default QoS policies that can be set once for multiple tests
 * This class also contains QoS policies that will be reset when a new configuration is written
 *
 */
class QoSManager {
  public:
    typedef void (*config_fn)(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration);

    struct QoSFunction {
        QoSFunction() = default;
        QoSFunction(config_fn function_, const int64_t configuration_)
            : function{function_}, configuration{configuration_} {}
        void operator()(config_fn function_, const int64_t configuration_) {
            function = function_;
            configuration = configuration_;
        }
        void applicable(const bool writer_, const bool reader_, const bool writeTopic_, const bool readTopic_) {
            writer = writer_;
            reader = reader_;
            writeTopic = writeTopic_;
            readTopic = readTopic_;
        }
        config_fn function;
        int64_t configuration;
        bool writer = true;
        bool reader = true;
        bool writeTopic = false;
        bool readTopic = false;
    };
    QoSManager();
    ~QoSManager();

    dds_qos_t *getWriter() const { return _customWriter; }
    dds_qos_t *getReader() const { return _customReader; }
    dds_qos_t *getWriteTopic() const { return _customWriteTopic; }
    dds_qos_t *getReadTopic() const { return _customReadTopic; }
    dds_listener_t *getListener() const { return _customListener; }

    void resetDefaultQoS();

    template <typename... ConfigFunctions>
    void configureDefaultQoS(ConfigFunctions... configFunctions);

    template <typename... ConfigFunctions>
    void configure(ConfigFunctions... configFunctions);

  private:
    dds_qos_t *_defaultWriter;
    dds_qos_t *_defaultReader;
    dds_qos_t *_defaultWriteTopic;
    dds_qos_t *_defaultReadTopic;
    dds_listener_t *_defaultListener;

    dds_qos_t *_customWriter;
    dds_qos_t *_customReader;
    dds_qos_t *_customWriteTopic;
    dds_qos_t *_customReadTopic;
    dds_listener_t *_customListener;

    void allocateDefaultQoS();
    void deallocateDefaultQoS();
    void allocateCustomQoS();
    void deallocateCustomQoS();
    void copyDefaultToCustom();

    static void configureQoS(dds_qos_t *qosWriter, dds_qos_t *qosReader, dds_qos_t *qosWriteTopic,
                             dds_qos_t *qosReadTopic, dds_listener_t *listener, QoSFunction configurationFunction);
};

template <typename... ConfigFunctions>
void QoSManager::configureDefaultQoS(ConfigFunctions... configFunctions) {
    // Contains initialisation of the different QoS policies
    ((configureQoS(_defaultWriter, _defaultReader, _defaultWriteTopic, _defaultReadTopic, _defaultListener,
                   configFunctions)),
     ...);
}

template <typename... ConfigFunctions>
void QoSManager::configure(ConfigFunctions... configFunctions) {
    deallocateCustomQoS();
    allocateCustomQoS();

    copyDefaultToCustom();

    ((configureQoS(_customWriter, _customReader, _customWriteTopic, _customReadTopic, _customListener,
                   configFunctions)),
     ...);
}

#endif
