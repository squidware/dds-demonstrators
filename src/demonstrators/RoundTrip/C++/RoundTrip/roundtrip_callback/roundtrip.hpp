#ifndef ROUNDTRIP_H
#define ROUNDTRIP_H

#include "performance/performance_measurements.hpp"

#include "RoundTripData.h"
#include "dds/dds.h"

/**
 * @brief RoundTrip contains the implementation of the roundtrip
 *
 * @note There can only be one instance of this class!
 */
class RoundTrip {
  public:
    RoundTrip(const unsigned int id, const unsigned int totalDevices, const unsigned int roundTrips);
    ~RoundTrip();

    bool running() const { return _running; }
    int getTime() const { return _timer.getTime<std::chrono::microseconds>(); }

    void run();

  private:
    const unsigned int _id;
    const unsigned int _totalDevices;
    static unsigned int _totalRoundtrips;

    static bool _running;
    static unsigned int _roundTrips;
    static RoundTripData_Msg _msg;
    static dds_entity_t _writer;
    dds_entity_t _reader;
    dds_entity_t _participant;
    dds_entity_t _topicWrite, _topicRead;
    dds_listener_t *_listener;

    static PerformanceMeasurements _timer;

    void initialise();
    void initiateRoundtrip();
    static void callBack(dds_entity_t reader, void *arg);
};

#endif // ROUNDTRIP_H