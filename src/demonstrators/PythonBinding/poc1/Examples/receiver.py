from sys import path
path.append("..")
from binding import Topic, Writer, Participant, DDSKeyValue, Listener, Reader
import time
import logging
import ctypes

logging.getLogger().setLevel(logging.INFO)

participant = Participant(27)
topic = Topic("testtopic", participant)

reader = Reader(participant, topic)

while True:
    msg = reader(DDSKeyValue)
    logging.info(msg)
    time.sleep(3)