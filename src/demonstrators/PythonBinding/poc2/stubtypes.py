"""
This file contains pre-defined descriptor ctypes.
"""

import ctypes


class KeyDescriptor(ctypes.Structure):
    """
    This class is used as an entry in the key descriptor list.
    """
    _fields_=[
        ("m_name", ctypes.c_char_p),
        ("m_index", ctypes.c_uint32)
    ]


class TopicDescriptor(ctypes.Structure):
    """
    This is the ctype of the topic descriptor.
    """
    _fields_=[
        ("m_size", ctypes.c_uint32),
        ("m_align", ctypes.c_uint32),
        ("m_flagset", ctypes.c_uint32),
        ("m_nkeys", ctypes.c_uint32),
        ("m_typename", ctypes.c_char_p),
        ("m_keys", ctypes.POINTER(KeyDescriptor)),
        ("m_nops", ctypes.c_uint32),
        ("m_ops", ctypes.POINTER(ctypes.c_uint32)),
        ("m_meta", ctypes.c_char_p)
    ]

class SampleInfo(ctypes.Structure):
    _fields_ = [('sample_state', ctypes.c_uint),
                ('view_state', ctypes.c_uint),
                ('instance_state', ctypes.c_uint),
                ('valid_data', ctypes.c_bool),
                ('source_timestamp', ctypes.c_int64),
                ('instance_handle', ctypes.c_uint64),
                ('pubblication_handle', ctypes.c_uint64),
                ('disposed_generation_count', ctypes.c_uint32),
                ('no_writer_generation_count', ctypes.c_uint32),
                ('sample_rank', ctypes.c_uint32),
                ('generation_rank', ctypes.c_uint32),
                ('absolute_generation_rank', ctypes.c_uint32)]