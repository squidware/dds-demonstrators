#include "DDSCommunication.hpp"
/**
 * @brief Construct a new DDSCommunication::DDSCommunication object
 * 
 * DDS some time to stabilize therefore a 2-second sleep is built-in after
 * initiating the variables.
 * 
 * @param id Id of the matrix board, used as the topic name.
 */
DDSCommunication::DDSCommunication(int id):
Communication(std::to_string(id)),
_dds_participant{dds_create_participant(DDS_DOMAIN_DEFAULT, NULL, NULL)},
_topic_network{dds_create_topic(_dds_participant, &Network_Message_desc, "network", NULL, NULL)},
_topic_write{dds_create_topic(_dds_participant, &Traffic_Message_desc, 
("b"+std::to_string(id)).c_str(), NULL, NULL)},
_network_reader{dds_create_reader(_dds_participant, _topic_network, NULL, NULL)},
_writer{dds_create_writer(_dds_participant, _topic_write, NULL, NULL)},
_network_writer{dds_create_writer(_dds_participant, _topic_network, NULL, NULL)},
_boards{0}
{  
    std::this_thread::sleep_for(std::chrono::seconds(2));
}
/**
 * @brief Function which establishes connection to the network
 * 
 * DDS is connected to the network after the constructor has finished.
 * The interface function is called connect to the network and in other
 * protocols connect to the network and send out a message of its connection.
 * As DDS is already connected, only the message gets sent out.
 * 
 * @param init_msg initialisation message containing network struct with type connected.
 * @param id Id of the matrix board.
 */
void DDSCommunication::connectToNetwork(Network_Message init_msg, int id){
    Clock::time_point start_time = Clock::now();
    std::string end_time_string = _test.getMiliFromTime(start_time);
    std::thread write_measurements_thread(&Test::WriteToFile, &_test, " ; ; ;startupend;"+
    end_time_string, "connected_"+std::to_string(id)+".csv");
    write_measurements_thread.detach();
    sendNetworkMessage(init_msg);
}
/**
 * @brief Function to receive messages from the DDS network.
 * 
 * This function checks each topic for messages, returning
 * The message and the topic it was found on.
 * If there are two board subscriptions, the application receives a lot of messages.
 * There would be a message available on both board topics at any almost any time.
 * Therefore the first checked topic is switched when a message is received from that topic.
 * This allows the application to be able to handle messages of both topics.
 * Otherwise the topics messages checked first would be handled more than the other.
 * 
 * @param id Id of the matrix board.
 * @return MessageReturnStruct struct containing received message.
 */
MessageReturnStruct DDSCommunication::receive(int id){ 
    MessageReturnStruct return_struct;
    dds_return_t read_return_values;
    dds_sample_info_t info[1];
    void *network_sample [1];
    network_sample[0] = Network_Message__alloc();  
    while(true){
        read_return_values = dds_take(_network_reader, network_sample, info, 1, 1);
        if(info->valid_data && (read_return_values > 0)){
            return_struct.topic = "network";
            return_struct.network_struct = *static_cast<Network_Message *>(network_sample[0]);
            return_struct.network_struct.address = strdup(return_struct.network_struct.address);
            Network_Message_free(network_sample[0], DDS_FREE_ALL);
            std::thread write_measurements_thread(&Test::WriteMeasurement, &_test, 
            return_struct.network_struct.message_id, return_struct.network_struct.id, id, 
            return_struct.topic, Clock::now(), 2);
            write_measurements_thread.detach();
            return return_struct;
        }
        if(_boards == 1){
            return_struct = checkReceiveTrafficMessage(_board1_reader, id, _topic_board1_name);
            if(return_struct.topic != "none"){
                return return_struct;
            }
        }
        else if(_boards > 1){
            if( _last == 1){
                return_struct = checkReceiveTrafficMessage(_board2_reader, id, _topic_board2_name);
                if(return_struct.topic != "none"){
                    _last  = 2;
                    return return_struct;
                }
                return_struct = checkReceiveTrafficMessage(_board1_reader, id, _topic_board1_name);
                if(return_struct.topic != "none"){
                    _last  = 1;
                    return return_struct;
                }
            }
            else{
                return_struct = checkReceiveTrafficMessage(_board1_reader, id, _topic_board1_name);
                if(return_struct.topic != "none"){
                    _last  = 1;
                    return return_struct;
                }
                return_struct = checkReceiveTrafficMessage(_board2_reader, id, _topic_board2_name);
                if(return_struct.topic != "none"){
                    _last  = 2;
                    return return_struct;
                }
            }
        }
    }
}
/**
 * @brief This function checks if there is a traffic message available on a specified reader.
 * 
 * The function checks if a message is available if so it will return it.
 * If not it will return the struct with a topic name none.
 * This indicates that there was no message available.
 * 
 * @param reader Reader entity on a specific topic.
 * @param id ID of the matrix board.
 * @param topic Topic name which is checked.
 * @return MessageReturnStruct struct containing received message.
 */
MessageReturnStruct DDSCommunication::checkReceiveTrafficMessage(dds_entity_t reader, int id, std::string topic){
    void *traffic_sample [1];
    traffic_sample[0] = Traffic_Message__alloc();  
    dds_sample_info_t info[1];
    MessageReturnStruct return_struct;
    dds_return_t read_return_values;
    read_return_values = dds_take(reader, traffic_sample, info, 1, 1);
    checkDDSError(read_return_values);
    return_struct.topic = "none";
    if(info->valid_data && (read_return_values > 0)){
        return_struct.topic = topic;
        return_struct.traffic_struct = *static_cast<Traffic_Message *>(traffic_sample[0]);             
        std::thread write_measurements_thread(&Test::WriteMeasurement, &_test, 
        return_struct.traffic_struct.message_id, return_struct.traffic_struct.id, 
        id, return_struct.topic, Clock::now(), 1);
        write_measurements_thread.detach();
    }
  
    Traffic_Message_free(traffic_sample[0], DDS_FREE_ALL);
    return return_struct;
}
/**
 * @brief Function to send a traffic message.
 * 
 * This function takes a traffic struct and publishes it to the writing topic of the matrix board.
 * 
 * @param msg Traffic struct to send.
 */
void DDSCommunication::sendTrafficMessage(Traffic_Message msg){
    msg.message_id =  getMessageID();
    std::thread write_measurements_thread(&Test::WriteMeasurement, &_test, msg.message_id,
    msg.id, -1, std::to_string(msg.id), Clock::now(), 3);
    write_measurements_thread.detach();
    dds_return_t test = dds_write(_writer, &msg);
    checkDDSError(test);
}

/**
 * @brief Function to send a network message.
 * 
 * This function takes a network struct and publishes it to the network matrix board topic.
 * 
 * @param msg Network struct to send.
 */
void DDSCommunication::sendNetworkMessage(Network_Message msg){
    msg.message_id =  getMessageID();
    std::thread write_measurements_thread(&Test::WriteMeasurement, &_test, 
    msg.message_id, msg.id, -1, "network", Clock::now(), 4);
    write_measurements_thread.detach();
    std::string address = "a";
    msg.address = &address[0];
    dds_write(_network_writer, &msg);
}
/**
 * @brief Function to subscribe to a topic.
 * 
 * In DDS this function only uses the topic name to subscribe to this topic.
 * If there is one board, the board 1 reader will subscribe to this topic,
 * if there are two boards the board 2 readers will subscribe to this topic.
 *
 * @param topic Name of the topic to subscribe to.
 * @param address Not used.
 */
void DDSCommunication::subscribe(std::string topic, std::string address){
    if(_boards == 0){
        _topic_board1_name = topic;
        _topic_board1 = dds_create_topic(_dds_participant, &Traffic_Message_desc,
        ("b"+topic).c_str(), NULL, NULL);
        _board1_reader = dds_create_reader(_dds_participant, _topic_board1, NULL, NULL);
    }
    else if(_boards == 1){
        _topic_board2_name = topic;
        _topic_board2 = dds_create_topic(_dds_participant, &Traffic_Message_desc,
         ("b"+topic).c_str(), NULL, NULL);
        _board2_reader = dds_create_reader(_dds_participant, _topic_board2, NULL, NULL);
    }
    _boards++;
}
/**
 * @brief Function to unsubscribe from a topic.
 * 
 * @param topic Name of the topic to unsubscribe from.
 */
void DDSCommunication::unsubscribe(std::string topic){
    if(_boards == 1){
        _boards--;
    }
    else if(_boards == 2){
        if(topic == _topic_board1_name){
            _topic_board1_name = _topic_board2_name;
            _topic_board1 = _topic_board2;
            _board1_reader = _board2_reader;
        }
        _boards--;
    }
}
/**
 * @brief Function to check if a DDS error has occurred.
 * 
 * @param value return value of a DDS function.
 */
void DDSCommunication::checkDDSError(const int32_t value) {
    if (value < 0) {
        throw std::runtime_error(dds_strretcode(-value));
    }
}