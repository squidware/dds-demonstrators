#include "ZMQCommunication.hpp"

std::mutex ZMQCommunication::_addresses_mutex{};
/**
 * @brief Construct a new ZMQCommunication::ZMQCommunication object
 * 
 * Sets the address attributes, write and network topic and
 * initialises the ZMQ context and sockets.
 * 
 * @param id ID of the matrix board
 * @param server_address Network address of ZMQ proxy publish socket
 * @param server_publish_address Network address of ZMQ proxy subscribe socket
 * @param own_address Network address of this matrix board
 */
ZMQCommunication::ZMQCommunication(std::string id, std::string server_address, std::string server_publish_address, std::string own_address):
Communication(id), _server_address{server_address}, 
_server_publish_address{server_publish_address}, 
_own_address{own_address},  _topic_write{id},
_topic_network{"network"}, 
_context{}, _sub_socket{_context, ZMQ_SUB}, 
_pub_socket{_context, ZMQ_PUB}, _network_pub_socket{_context, ZMQ_PUB}
{
}
/**
 * @brief Function which connects the matrix board to the ZMQ network.
 * 
 * With ZMQ the first few sent messages never reach their destination.
 * Therefore this function continuously sends out network messages
 * and starts a thread looking for the message to pop up.
 * When it sees its own message it is connected and ready future messages
 * won't be missed. The messaging will then be stopped of course.
 * 
 * @param init_msg Initial network message to be sent.
 * @param id ID of matrix board.
 */
void ZMQCommunication::connectToNetwork(Network_Message init_msg, int id){
    pthread_t cThread;
    std::atomic<bool> connected(false);
    _sub_socket.set(zmq::sockopt::subscribe, "network");  
    _pub_socket.bind(_own_address);
    _network_pub_socket.connect(_server_publish_address);
    _sub_socket.connect(_server_address);

    InitConnectionArgStruct<ZMQCommunication> args{&connected, id, this};
    pthread_create(&cThread, NULL, initialConnectionCheckerThread, &args);
    while (connected == false){
        std::this_thread::sleep_for(std::chrono::seconds(2));
        sendNetworkMessage(init_msg);
    }
    pthread_join(cThread, NULL);
    Clock::time_point start_time = Clock::now();
    std::string end_time_string = _test.getMiliFromTime(start_time);
    std::thread write_measurements_thread(&Test::WriteToFile, &_test, 
    " ; ; ;startupend;"+end_time_string, "rdy.csv");
    write_measurements_thread.detach();
}
/**
 * @brief Thread wich checks if an initial message of the matrix board has reached the network
 * 
 * This function checks the proxy socket for the message sent out by the connection to network function.
 * When the message pops up it sets the connected boolean to true and is done.
 * The connected boolean will stop messaging and notify the object its connected.
 * 
 * @param thread_arg init connection arg struct containing the address of connected boolean,
 * matrix board id and the communication object.
 * @return void* 
 */
void *ZMQCommunication::initialConnectionCheckerThread(void* thread_arg){   
    InitConnectionArgStruct<ZMQCommunication>* args = static_cast<InitConnectionArgStruct<ZMQCommunication>*>(thread_arg);
    std::atomic<bool>* connected = args->connected;
    MessageReturnStruct message_vals;

    while (*connected == false){
        message_vals = args->communication->receive(args->id);
        if(message_vals.topic == "network"){
            if(message_vals.network_struct.id == args->id)
            {
               *connected = true; 
            }
        }   
    }
    pthread_exit(NULL);
}
/**
 * @brief Function to send out traffic message
 * 
 * The function first sends the topic with a send more flag.
 * This way ZMQ knows this is the topic. Then the traffic message
 * struct is sent.
 * 
 * @param msg Traffic message struct to be sent
 */
void ZMQCommunication::sendTrafficMessage(Traffic_Message msg){
    msg.message_id =  getMessageID();
    std::thread write_measurements_thread(&Test::WriteMeasurement, &_test, msg.message_id, 
    msg.id, -1, std::to_string(msg.id), Clock::now(), 3);
    write_measurements_thread.detach();
    _pub_socket.send(generateMessageFromString(_topic_write), zmq::send_flags::sndmore);
    _pub_socket.send(generateMessageFromStruct<Traffic_Message>(msg), zmq::send_flags::none);
}
/**
 * @brief Function to send a network message to the proxy
 * 
 * The function first sends the topic with a send more flag.
 * This way ZMQ knows this is the topic. Then the network message
 * struct is sent. As the char pointer in the struct is not compatible
 * with ZMQ the address is then sent as this value apart from the struct.
 * 
 * @param msg Network message struct to be sent
 */
void ZMQCommunication::sendNetworkMessage(Network_Message msg){
    msg.message_id =  getMessageID();
    std::thread write_measurements_thread(&Test::WriteMeasurement, &_test, msg.message_id,
     msg.id, -1, _topic_network, Clock::now(), 4);
    write_measurements_thread.detach();
    _network_pub_socket.send(generateMessageFromString(_topic_network), zmq::send_flags::sndmore);
    _network_pub_socket.send(generateMessageFromStruct<Network_Message>(msg), zmq::send_flags::sndmore);
    _network_pub_socket.send(generateMessageFromString(_own_address), zmq::send_flags::none);
}
/**
 * @brief Function to receive messages from the subscribed to topics.
 * 
 * ZMQ uses a message queue for receiving its messages.
 * The recv function blocks till there is a message available.
 * After this, the message information will be extracted and returned.
 * 
 * @param id ID of the matrix board.
 * @return MessageReturnStruct struct containing the information of the received message.
 */
MessageReturnStruct ZMQCommunication::receive(int id){
    MessageReturnStruct return_struct;
    zmq::message_t topic;
    zmq::message_t address;
    zmq::message_t msg_val;
    zmq::recv_result_t res_topic = _sub_socket.recv(topic, zmq::recv_flags::none);
    return_struct.topic = std::string(static_cast<char *>(topic.data()), topic.size()); 
    zmq::recv_result_t res_struct = _sub_socket.recv(msg_val, zmq::recv_flags::none);
  
    res_topic.value();
    res_struct.value();
    if(return_struct.topic == "network"){
        zmq::recv_result_t res_address = _sub_socket.recv(address, zmq::recv_flags::none);
        std::string address_string = std::string(static_cast<char *>(address.data()), address.size()); 
        res_address.value();
        return_struct.network_struct = *static_cast<Network_Message*>(msg_val.data());
        std::thread write_measurements_thread(&Test::WriteMeasurement, &_test, 
        return_struct.network_struct.message_id, 
        return_struct.network_struct.id, id, return_struct.topic, Clock::now(), 2);
        write_measurements_thread.detach();
        return_struct.network_struct.address = strdup(address_string.c_str());
    }
    else{
        return_struct.traffic_struct = *static_cast<Traffic_Message*>(msg_val.data());  
        std::thread write_measurements_thread(&Test::WriteMeasurement, &_test, 
        return_struct.traffic_struct.message_id, 
        return_struct.traffic_struct.id, id, return_struct.topic, Clock::now(), 1);
        write_measurements_thread.detach();   
    }
    return return_struct;
}
/**
 * @brief Function to subscribe to a topic.
 * 
 * The function connects to the publish socket of another matrix board.
 * It then sets its subscription.
 * After this, the board is subscribed and can receive messages.
 * 
 * @param topic Name of the topic to subscribe to.
 * @param address The network address of the publish socket to connect to.
 */
void ZMQCommunication::subscribe(std::string topic, std::string address){
    std::lock_guard<std::mutex> lock(_addresses_mutex);
    _board_addresses[topic] = address;
    _sub_socket.set(zmq::sockopt::subscribe, topic);
    _sub_socket.connect(address);
}
/**
 * @brief Function to unsubscribe to a topic.
 * 
 * This function disconnects form the publish socket corresponding
 * with the name of the topic.
 * 
 * @param topic name of the topic to unsubscribe from.
 */
void ZMQCommunication::unsubscribe(std::string topic){
    std::lock_guard<std::mutex> lock(_addresses_mutex);
    _sub_socket.disconnect(_board_addresses[topic]);
    _board_addresses.erase(topic);
}
/**
 * @brief Function to turn a string into a ZMQ message.
 * 
 * @param message string to be sent with ZMQ.
 * @return zmq::message_t ZMQ message generated from the string.
 */
zmq::message_t ZMQCommunication::generateMessageFromString(std::string message) {
    zmq::message_t msg(message.size());
    memcpy(msg.data(), message.c_str(), message.size());
    return msg;
}
/**
 * @brief Function to generate a ZMQ message from a struct.
 * 
 * @tparam StructType Type of struct to be converted.
 * @param structure Structure to be converted.
 * @return zmq::message_t ZMQ message generated from the struct.
 */
template <typename StructType>
zmq::message_t ZMQCommunication::generateMessageFromStruct(StructType structure) {
    const unsigned int length = sizeof(StructType);
    zmq::message_t struct_msg(length);
    memcpy(struct_msg.data(), &structure, length);
    return struct_msg;
}
