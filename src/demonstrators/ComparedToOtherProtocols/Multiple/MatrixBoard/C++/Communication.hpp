#ifndef COMMUNICATION_HPP
#define COMMUNICATION_HPP

#include <string>
#include <map>
#include <vector>
#include <atomic>
#include <thread>
#include <cstring>
#include "build/MessageStructs.h"
#include "NetworkMessageType.hpp"
#include "MessageReturnStruct.hpp"
#include "InitConnectionArgsStruct.hpp"
#include "Test.hpp"

/**
 * @brief Communication abstract class for the matrix board application
 * 
 */

class Communication {
 protected:
  typedef std::chrono::high_resolution_clock Clock;
  std::atomic<int> _message_id = 0;
  Test _test;
  Communication(std::string id): _test(id){};
 public:
  virtual ~Communication() = default;
  virtual void connectToNetwork(Network_Message init_msg, int id) = 0;
  virtual void sendNetworkMessage(Network_Message msg) = 0;
  virtual void sendTrafficMessage(Traffic_Message msg) = 0;
  virtual MessageReturnStruct receive(int id) = 0;
  virtual void subscribe(std::string topic, std::string address) = 0;
  virtual void unsubscribe(std::string topic) = 0;
  int getMessageID(){ return _message_id.fetch_add(1); };
};

#endif