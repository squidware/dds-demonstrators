#include "Sensor.hpp"
/**
 * @brief Function which simulates traffic sensor
 * 
 * The function uses a random number generator to return
 * random sensor values.
 * 
 * @return int traffic intensity
 */
int Sensor::measure(){
    std::random_device seed;        // Will be used to obtain a seed for the random number engine
    std::mt19937 generator(seed()); // Standard mersenne_twister_engine seeded with seed()
    std::uniform_int_distribution<> distribution(1, 100);
    return distribution(generator);
}