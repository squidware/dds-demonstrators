#include "MQTTCommunication.hpp"
/**
 * @brief Construct a new MQTTCommunication::MQTTCommunication object.
 * 
 * Sets QoS, initializes MQTT client, the network topic and the writing topic.
 * 
 * @param id ID of the matrix board.
 * @param server_address Network address of the MQTT broker.
 * @param QoS Quality of standard level to be used by MQTT.
 */
MQTTCommunication::MQTTCommunication(std::string id, std::string server_address, int qos)
: Communication(id), _qos{qos}, _client{server_address, id}, _topic_network{_client, "network", qos}
, _topic_write{_client, id, qos} {
}
/**
 * @brief Function to connect to the MQTT network.
 * 
 * This function connects the client to the broker.
 * It starts the consuming of messages and subscribes to the network matrix
 * board topic with the option set to not receive messages which comef
 * from himself.
 * After that, a message is sent to the network to let the other
 * connected nodes know this matrix board is online.
 * 
 * @param init_msg Initial message to be sent to indicate liveliness.
 * @param id Id of the matrix board.
 */
void MQTTCommunication::connectToNetwork(Network_Message init_msg, int id){
    _client.connect()->wait();
    _client.start_consuming();
    _topic_network.subscribe(mqtt::subscribe_options(true))->wait();
    Clock::time_point start_time = Clock::now();
    std::string end_time_string = _test.getMiliFromTime(start_time);
    std::thread write_measurements_thread(&Test::WriteToFile, &_test, 
    " ; ; ;startupend;"+end_time_string, "str_dne.csv");
    write_measurements_thread.detach();
    sendNetworkMessage(init_msg);
}
/**
 * @brief Function to send a traffic message to the broker
 * 
 * As the library does not allow structs to be sent the message struct
 * is first turned into a CSV string before it is sent.
 * 
 * @param msg_struct The traffic struct to be sent
 */
void MQTTCommunication::sendTrafficMessage(Traffic_Message msg_struct){
    msg_struct.message_id =  getMessageID();
    std::thread write_measurements_thread(&Test::WriteMeasurement, &_test, msg_struct.message_id, 
    msg_struct.id, -1, std::to_string(msg_struct.id), Clock::now(), 3);
    write_measurements_thread.detach();
    std::string msg = trafficStructToString(msg_struct);
    _topic_write.publish(msg); 
}
/**
 * @brief Function to send a network message to the broker
 * 
 * As the library does not allow structs to be sent the message struct
 * is first turned into a CSV string before it is sent.
 * 
 * @param msg_struct The network struct to be sent
 */
void MQTTCommunication::sendNetworkMessage(Network_Message msg_struct){
    msg_struct.message_id =  getMessageID();
    std::thread write_measurements_thread(&Test::WriteMeasurement, &_test, msg_struct.message_id, 
    msg_struct.id, -1, "network", Clock::now(), 4);
    write_measurements_thread.detach();
    std::string msg = networkStructToString(msg_struct);
    _topic_network.publish(msg);    
}
/**
 * @brief Function to receive MQTT messages 
 * 
 * MQTT uses a queue for its messages.
 * The consume function will block thill there is a message available.
 * If so the contents of this message will be extracted, put into a struct
 * and finally be returned.
 * 
 * @param id 
 * @return MessageReturnStruct 
 */
MessageReturnStruct MQTTCommunication::receive(int id){
    mqtt::const_message_ptr msg;
    MessageReturnStruct return_struct;
    msg = _client.consume_message();
    std::string topic = msg->get_topic();
    std::string payload = msg->get_payload();

    return_struct.topic = topic;
    if(topic == "network"){
        return_struct.network_struct = stringToNetworkStruct(payload);
        std::thread write_measurements_thread(&Test::WriteMeasurement, &_test, 
        return_struct.network_struct.message_id, 
        return_struct.network_struct.id, id, return_struct.topic, Clock::now(), 2);
        write_measurements_thread.detach();
    }
    else {
        return_struct.traffic_struct = stringToTrafficStruct(payload);
        std::thread write_measurements_thread(&Test::WriteMeasurement, &_test, 
        return_struct.traffic_struct.message_id, 
        return_struct.traffic_struct.id, id, return_struct.topic, Clock::now(), 1);
        write_measurements_thread.detach();
    }

    return return_struct;

}
/**
 * @brief Function to subscribe to a topic
 * 
 * Subscribes to a topic based on the inputted topic name,
 * waits for the process to finish. 
 * 
 * @param topic Name of the topic to subscribe to
 * @param address not used
 */
void MQTTCommunication::subscribe(std::string topic, std::string address){
    _client.subscribe(topic, _qos)->wait();
}
/**
 * @brief Function to unsubscribe from a topic
 * 
 * @param topic Name of the topic to unsubscribe from
 */
void MQTTCommunication::unsubscribe(std::string topic){
    _client.unsubscribe(topic)->wait();
}
/**
 * @brief Function to turn csv string to a traffic struct.
 * 
 * @param data_string String to turn into a traffic struct.
 * @return Traffic_Message traffic struct containing information of the message.
 */
Traffic_Message MQTTCommunication::stringToTrafficStruct(std::string data_string){
    std::vector<std::string> results = csvStringToVector(data_string);
    Traffic_Message string_struct{std::stoi(results[0]), 
    std::stoi(results[1]), std::stoi(results[2])};
    return string_struct;
}
/**
 * @brief Function to turn csv string to a network struct.
 * 
 * @param data_string String to turn into a traffic struct.
 * @return Traffic_Message network struct containing information of the message.
 */
Network_Message MQTTCommunication::stringToNetworkStruct(std::string data_string){
    std::vector<std::string> results = csvStringToVector(data_string);
    Network_Message string_struct{std::stoi(results[0]), 
    static_cast<NetworkMessageType>(std::stoi(results[1])), 
    std::stoi(results[2]), strdup(" ")};
    return string_struct;
}
/**
 * @brief Function to turn a traffic struct into a csv string.
 * 
 * @param traffic_struct Traffic struct to be converted to a string.
 * @return std::string CSV string containing the information of the traffic struct.
 */
std::string MQTTCommunication::trafficStructToString(Traffic_Message traffic_struct){
    std::string struct_string = std::to_string(traffic_struct.message_id)+","+
    std::to_string(traffic_struct.id)+","+std::to_string(traffic_struct.traffic);
    return struct_string;
}
/**
 * @brief Function to turn a network struct into a csv string.
 * 
 * @param network_struct Network struct to be converted to a string.
 * @return std::string CSV string containing the information of the network struct.
 */
std::string MQTTCommunication::networkStructToString(Network_Message network_struct){
    std::string struct_string = std::to_string(network_struct.message_id)+","+
    std::to_string(network_struct.type)+","+std::to_string(network_struct.id)+ ", "; 
    return struct_string;
}
/**
 * @brief Function to tun a csv string into a vector.
 * 
 * @param data_string CSV string to be turned into a vector.
 * @return std::vector<std::string> vector containing strings of the csv string.
 */
std::vector<std::string> MQTTCommunication::csvStringToVector(std::string data_string){
    std::vector<std::string> result;
    std::stringstream ss(data_string);
    while(ss.good())
    {
        std::string substr;
        getline(ss, substr, ',');
        result.push_back(substr);
    }
    return result;
}
