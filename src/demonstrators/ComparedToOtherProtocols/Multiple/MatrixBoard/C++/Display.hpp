#ifndef DISPLAY_HPP
#define DISPLAY_HPP

#include "Sign.hpp"

/**
 * @brief Class representing matrix board display.
 * 
 */
class Display
{
private:
    Sign _sign;

public:
    Display();
    ~Display() = default;
    void setSign(Sign sign);
    Sign getSign();
};

#endif