#ifndef DDSCOMMUNICATION_HPP
#define DDSCOMMUNICATION_HPP

#include "dds/dds.h"
#include "Communication.hpp"

/**
 * @brief The DDS communicatio class for the matrixboard application
 * Implements the abstract communication class.
 * 
 */

class DDSCommunication: public Communication{
private:
    dds_entity_t _dds_participant;
    dds_entity_t _topic_network;
    dds_entity_t _topic_write;
    dds_entity_t _topic_board1;
    dds_entity_t _topic_board2;
    std::string  _topic_board1_name;
    std::string  _topic_board2_name;
    dds_entity_t _network_reader;
    dds_entity_t _board1_reader;
    dds_entity_t _board2_reader;
    dds_entity_t _writer; 
    dds_entity_t _network_writer; 
    std::string  _id;
    std::atomic<int> _boards;
    int _last = 0;

public:
    DDSCommunication(int id);
    ~DDSCommunication() = default;
    void connectToNetwork(Network_Message initMsg, int id);
    void sendTrafficMessage(Traffic_Message msg);
    void sendNetworkMessage(Network_Message msg);
    MessageReturnStruct checkReceiveTrafficMessage(dds_entity_t reader, int id, std::string topic);
    MessageReturnStruct receive(int id);
    void subscribe(std::string topic, std::string address = "");
    void unsubscribe(std::string topic);
    void checkDDSError(const int32_t value);
};
#endif