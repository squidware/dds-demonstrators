#ifndef TEST_HPP
#define TEST_HPP

#include <fstream>
#include <sstream>
#include <string>
#include <chrono>
#include <vector>
#include <mutex>

/**
 * @brief The Test class is used to write measurements done
 * in the application to a file.
 * 
 */
class Test
{

typedef std::chrono::high_resolution_clock Clock;

private:
    std::string _traffic_received_file_name;
    std::string _network_received_file_name;
    std::string _traffic_sent_file_name;
    std::string _network_sent_file_name;
    static std::mutex _write_network_received_mutex;
    static std::mutex _write_traffic_received_mutex;
    static std::mutex _write_network_sent_mutex;
    static std::mutex _write_traffic_sent_mutex;

public:
    Test(std::string filename_id);
    ~Test() = default;
    void initFiles();
    void WriteMeasurement(int message_id, int sent_by, int received_by, std::string topic, Clock::time_point time, int type);
    void WriteToFile(std::string line, std::string file_name);
    std::string getTimeDifference(Clock::time_point start_time, Clock::time_point end_time);
    std::string getMiliFromTime(Clock::time_point time);
};

#endif