#ifndef MATRIXBOARD_HPP
#define MATRIXBOARD_HPP

#include "Communication.hpp"
#include "Sensor.hpp"
#include "Display.hpp"
#include "Test.hpp"
#include <pthread.h>
#include <type_traits>
#include <mutex>
#include <map>
#include <chrono>
#include <algorithm>


/**
 * @brief Matrix board class representing the matrix board system
 * 
 * @tparam CommunicationType type(protocol) of communcation that will be used
 */


template <typename CommunicationType>
class MatrixBoard {

  static_assert(std::is_base_of<Communication, CommunicationType>::value && "Communication Type does not have the Communication base class!");
  typedef std::chrono::high_resolution_clock Clock;

  private:
    CommunicationType* _communication;
    int _id;
    Sensor _sensor;
    Display _display;
    Test _test;
    std::map<int, int> _next_boards;
    Clock::time_point last_message_time_board1;
    Clock::time_point last_message_time_board2;
    static std::mutex _boards_mutex;

  public:
    MatrixBoard(CommunicationType* initialized_object, int id);
    ~MatrixBoard() =  default;
    int run();
    void addBoard(int id, std::string address);
    void deleteBoard(int id);
    void determineSign();
    void determineSubscribtion(int id, std::string address);
    void deleteNetworkStructFromVector(Network_Message structure);
    void deleteTrafficStructFromVector(Traffic_Message structure);
    static void *trafficMessagerThread(void* thread_arg);
    static void *handleNetworkMessageThread(void* thread_arg);
    static void *handleTrafficMessageThread(void* thread_arg);
    static void *livelinessCheckerThread(void* thread_arg);
    static void *runThread(void* thread_arg);
    
};

template <typename CommunicationType> 
std::mutex MatrixBoard<CommunicationType>::_boards_mutex{};


template <typename CommunicationType> 
struct NetworkArgStruct { 
  Network_Message message; 
  MatrixBoard<CommunicationType>* board; 
};
template <typename CommunicationType> 
struct TrafficArgStruct {
  const char* topic; 
  Traffic_Message message; 
  MatrixBoard<CommunicationType>* board; 
}; 


/**
 * @brief Construct a new Matrix Board< Communication Type>:: Matrix Board object
 * 
 * @tparam CommunicationType type(protocol) of communication that will be used.
 * @param initialized_object The communication object.
 * @param id The id of the matrix board
 */
template <typename CommunicationType>
MatrixBoard<CommunicationType>::MatrixBoard(CommunicationType* initialized_object, int id):
_communication{initialized_object}, _id{id}, _test{"matrixboard_time_tests_"+std::to_string(id)}{
}


/**
 * @brief Run function to run the logic of the matrix board system
 * 
 * The run function starts the messaging thread and handles the incoming message.
 * Starting threads to handle these messages accordingly.
 * 
 * @tparam CommunicationType 
 */
template <typename CommunicationType>
int MatrixBoard<CommunicationType>::run(){
    pthread_attr_t threadAttr;
    pthread_t run_thread;
    int error = 0;
    pthread_attr_init(&threadAttr);
    sched_param priority;
    priority.sched_priority = 99;
    pthread_attr_setinheritsched(&threadAttr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(&threadAttr, SCHED_FIFO);
    pthread_attr_setschedparam(&threadAttr, &priority);
    error = pthread_create(&run_thread, &threadAttr, runThread, this);
    pthread_join(run_thread, NULL);
    return error;
}

template <typename CommunicationType>
void *MatrixBoard<CommunicationType>::runThread(void* thread_arg){
    MatrixBoard<CommunicationType>* board = static_cast<MatrixBoard<CommunicationType>*>(thread_arg);
    pthread_attr_t threadAttr;
    pthread_t tThread;
    pthread_t lThread;
    pthread_t thThread;
    pthread_t nhThread;
    pthread_attr_init(&threadAttr);
    sched_param priority;
    priority.sched_priority = 99;
    Network_Message init_msg{0, CONNECTED, board->_id, NULL};
    board->_communication->connectToNetwork(init_msg, board->_id);
    pthread_attr_setinheritsched(&threadAttr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(&threadAttr, SCHED_FIFO);
    pthread_attr_setschedparam(&threadAttr, &priority);
    pthread_create(&tThread, &threadAttr, trafficMessagerThread, board);
    pthread_detach(tThread);
    pthread_create(&lThread, &threadAttr, livelinessCheckerThread, board);
    pthread_detach(lThread);


    while(true){
        MessageReturnStruct return_vals = board->_communication->receive(board->_id);
        if(return_vals.topic == "network"){
            NetworkArgStruct<CommunicationType>* args = (NetworkArgStruct<CommunicationType>*) malloc(sizeof(NetworkArgStruct<CommunicationType>)); 
            args->message = return_vals.network_struct;
            args->board =  board; 
            pthread_create(&nhThread, &threadAttr, handleNetworkMessageThread, args);
            pthread_detach(nhThread);
        }
        else{
            TrafficArgStruct<CommunicationType>* args = (TrafficArgStruct<CommunicationType>*) malloc(sizeof(TrafficArgStruct<CommunicationType>)); 
            args->topic = return_vals.topic.c_str();
            args->message = return_vals.traffic_struct;
            args->board =  board;
            pthread_create(&thThread, &threadAttr, handleTrafficMessageThread, args);
            pthread_detach(thThread);
            std::lock_guard<std::mutex> board_lock(_boards_mutex);
            if(board->_next_boards.size() > 0){
                if(std::stoi(return_vals.topic) == ((board->_next_boards).begin())->first){
                    board->last_message_time_board1 = Clock::now();
                }
                else if(std::stoi(return_vals.topic) == (--(board->_next_boards).end())->first){
                    board->last_message_time_board2 = Clock::now();
                }
            }
        }   
        std::this_thread::sleep_for(std::chrono::milliseconds(1)); 
    }
    return NULL;
}


/**
 * @brief The add board function is used to add a board the boards that the current matrix board
 * is watching for information
 * 
 * @tparam CommunicationType 
 * @param id ID of the board to be added
 * @param address Network address of the board to be added
 */
template <typename CommunicationType>
void MatrixBoard<CommunicationType>::addBoard(int id, std::string address){
    _communication->subscribe(std::to_string(id), address);
    _next_boards[id] = 0;
}
/**
 * @brief The delete board function is used to delete a board that the current matrix board
 * is watching for information
 * 
 * @tparam CommunicationType 
 * @param id ID of the board to be deleted
 */
template <typename CommunicationType>
void MatrixBoard<CommunicationType>::deleteBoard(int id){
    _communication->unsubscribe(std::to_string(id));
    _next_boards.erase(id);   
}
/**
 * @brief The determine sign function determines which sign should be shown based on
 * traffic intensity
 * 
 * @tparam CommunicationType 
 */
template <typename CommunicationType>
void MatrixBoard<CommunicationType>::determineSign(){
    int total = (--_next_boards.end())->second;
    if(_next_boards.size() > 0){
       total =+(_next_boards.begin())->second;
    }
    if(_next_boards.size() > 1){
       total =+ (--_next_boards.end())->second;
    }
    Sign sign = static_cast<Sign>((total/static_cast<int>(_next_boards.size()))%10);
    _display.setSign(sign); 

}
/**
 * @brief The determine subscription function determines whether the matrix board should subscribe to another.
 * 
 * The function determines whether the board is a downstream board.
 * Besides this, it also checks if this board is closer than the currently subscribed to boards if there are two subscriptions.
 * If the board is subscribed to one board or less it has to subscribe.
 * The function adds or deletes boards accordingly using the add and delete board functions.
 * 
 * @tparam CommunicationType 
 * @param id ID of the matrix board to check
 * @param address Address of the matrix board to check
 */
template <typename CommunicationType>
void MatrixBoard<CommunicationType>::determineSubscribtion(int id, std::string address){
    std::lock_guard<std::mutex> guard(_boards_mutex);
    if(_next_boards.find(id) == _next_boards.end()){
        if(_next_boards.size() < 2 && _id < id){
            addBoard(id, address);
        }
        else if(_next_boards.size() > 0){
            if(id < (--_next_boards.end())->first && _id < id) {
                deleteBoard((--_next_boards.end())->first);
                addBoard(id, address);
            }
        }
    }
}
/**
 * @brief Thread for sending out messages
 * 
 * The thread uses the communication objectives of the current matrix board object.
 * This is used to send out traffic messages.
 * The sleep of 24 milliseconds is there to comply with the minimum gap of
 * 24 milliseconds between traffic messages.
 * 
 * @tparam CommunicationType 
 * @param thread_arg Address of current matrix board
 * @return void* 
 */
template <typename CommunicationType>
void *MatrixBoard<CommunicationType>::trafficMessagerThread(void* thread_arg){
    MatrixBoard<CommunicationType>* board = static_cast<MatrixBoard<CommunicationType>*>(thread_arg);
    Traffic_Message msg_struct;
    std::string message_ready_time_string = "";
    while(true){
        Clock::time_point start_time = Clock::now();
        msg_struct.id = board->_id;
        msg_struct.traffic = board->_sensor.measure();
        board->_communication->sendTrafficMessage(msg_struct);
        Clock::time_point end_time = Clock::now();     
        message_ready_time_string = board->_test.getTimeDifference(start_time, end_time);
        std::thread write_measurements_thread(&Test::WriteToFile, &board->_test, " ; ; ;trafficmessage_ready;"+message_ready_time_string, "trafficmessage_ready.csv");
        write_measurements_thread.detach();
        std::this_thread::sleep_for(std::chrono::milliseconds(24));
    }
    pthread_exit(NULL);
}
/**
 * @brief Thread that handles a network message.
 * 
 * The thread holds the logic needed to deal with a network message.
 * It does not process its own messages.
 * Furthermore it determines which functions to run based on the network message type.
 * If a new board connects it the current board will notify its liveliness to the system
 * So that the new board may connect to the current board.
 * Besides this, it will also check if the new board should be subscribed to.
 * If The board receives a message that contained information that another board is online,
 * it will determine if it should subscribe to it.
 * When a message that contains information that a board disconnected is processed the thread
 * will handle this checking its subscriptions and deleting the subscription if necessary.
 * Besides this, if the board was an upstream board the board will notify the system of its liveliness.
 * This is done so that boards who lost a subscription might find this board to connect to.
 * 
 * @tparam CommunicationType 
 * @param thread_arg Address of network arg struct containing the board object and the network message
 * @return void* 
 */
template <typename CommunicationType>
void *MatrixBoard<CommunicationType>::handleNetworkMessageThread(void* thread_arg){
    NetworkArgStruct<CommunicationType>* args = static_cast<NetworkArgStruct<CommunicationType>*>(thread_arg);
    MatrixBoard<CommunicationType>* board = args->board;
    Network_Message message = args->message;
    if(message.id != board->_id){
        if(message.type == CONNECTED){
            Clock::time_point start_time = Clock::now();
            Network_Message liveliness_message{1, ONLINE, board->_id, NULL};
            board->_communication->sendNetworkMessage(liveliness_message);
            Clock::time_point end_time = Clock::now();
            std::string message_ready_time_string = board->_test.getTimeDifference(start_time, end_time);
            std::thread write_measurements_thread(&Test::WriteToFile, &board->_test, " ; ; ;livelinessready_connected;"+message_ready_time_string, "nt_ready.csv");
            write_measurements_thread.detach();
            board->determineSubscribtion(message.id, message.address);

        }
        else if(message.type == ONLINE){
            board->determineSubscribtion(message.id, message.address);
        }
        else if(message.type == DISCONNECTED){
            std::lock_guard<std::mutex> boards_lock(_boards_mutex);
            if(board->_next_boards.find(message.id) != board->_next_boards.end()){
                board->deleteBoard(message.id);
            }
            if(message.id < board->_id){
                Clock::time_point start_time = Clock::now();
                Network_Message liveliness_message{1, ONLINE, board->_id, NULL};
                board->_communication->sendNetworkMessage(liveliness_message);
                Clock::time_point end_time = Clock::now();
                std::string message_ready_time_string = board->_test.getTimeDifference(start_time, end_time);
                std::thread write_measurements_thread(&Test::WriteToFile, &board->_test, " ; ; ;livelinessready_disconnected;"+message_ready_time_string, "nt_ready.csv");
                write_measurements_thread.detach();
            }
        }
    }
    free(args->message.address);
    free(args);
    pthread_exit(NULL);
}
/**
 * @brief Thread to handle a traffic message
 * 
 * This thread sets the traffic intensity of the subscribed to board.
 * It checks if the subscription is still valid.
 * After that, it starts the logic to determine the sign as the traffic
 * intensity values are updated
 * 
 * 
 * @tparam CommunicationType 
 * @param thread_arg Address of traffic arg struct containing the matrix board object and the traffic message
 * @return void* 
 */
template <typename CommunicationType>
void *MatrixBoard<CommunicationType>::handleTrafficMessageThread(void* thread_arg){ 
    TrafficArgStruct<CommunicationType>* args = static_cast<TrafficArgStruct<CommunicationType>*>(thread_arg);
    MatrixBoard<CommunicationType>* board = args->board;
    Traffic_Message message = args->message;
    int topic = atoi(args->topic); 
    _boards_mutex.lock();
    if(board->_next_boards.find(topic) != board->_next_boards.end()){
        board->_next_boards[topic] = message.traffic;
        board->determineSign();
    }
    _boards_mutex.unlock();
    free(args);
    pthread_exit(NULL);
    return NULL;
}
/**
 * @brief Thread that checks the liveliness of the subscription boards
 * 
 * This thread checks if the board is still receiving messages from the subscribed to boards.
 * If there are no messages for two seconds it will consider the board disconnected and will notify
 * The network of this.
 * The two seconds were picked to comply with the 2-second guard specified by the Dutch government.
 * 
 * @tparam CommunicationType 
 * @param thread_arg 
 * @return void* 
 */
template <typename CommunicationType>
void *MatrixBoard<CommunicationType>::livelinessCheckerThread(void* thread_arg){    
    MatrixBoard<CommunicationType>* board = static_cast<MatrixBoard<CommunicationType>*>(thread_arg);
    while(true){
        std::this_thread::sleep_for(std::chrono::milliseconds(1)); 
        Clock::time_point now = Clock::now();
        std::lock_guard<std::mutex> guard(_boards_mutex);
        if(board->_next_boards.size()>0){
            int difference = std::chrono::duration_cast<std::chrono::seconds>(now - board->last_message_time_board1).count();
            if(difference > 2 && difference < 100){
                Clock::time_point start_time = Clock::now();
                Network_Message board_disconnect_message{1, DISCONNECTED, (board->_next_boards.begin())->first, NULL};
                board->deleteBoard((board->_next_boards.begin())->first);
                board->_communication->sendNetworkMessage(board_disconnect_message);
                Clock::time_point end_time = Clock::now();
                std::string message_ready_time_string = board->_test.getTimeDifference(start_time, end_time);
                std::thread write_measurements_thread(&Test::WriteToFile, &board->_test, " ; ; ;networkmessageready;"+message_ready_time_string, "nt_rdy.csv");
                write_measurements_thread.detach();
                board->last_message_time_board1 = std::chrono::time_point<Clock>{};
            }
        }
        if(board->_next_boards.size() > 1){
            int difference = std::chrono::duration_cast<std::chrono::seconds>(now - board->last_message_time_board2).count();
            if(difference > 2 && difference < 100){
                Clock::time_point start_time = Clock::now();
                Network_Message board_disconnect_message{1, DISCONNECTED, (--board->_next_boards.end())->first, NULL};
                board->deleteBoard((--board->_next_boards.end())->first);
                board->_communication->sendNetworkMessage(board_disconnect_message);
                Clock::time_point end_time = Clock::now();
                std::string message_ready_time_string = board->_test.getTimeDifference(start_time, end_time);
                std::thread write_measurements_thread(&Test::WriteToFile, &board->_test, " ; ; ;networkmessageready;"+message_ready_time_string, "nt_rdy.csv");
                write_measurements_thread.detach();
                board->last_message_time_board2 = std::chrono::time_point<Clock>{};
            }
        }   
    }
    pthread_exit(NULL);
}
#endif


