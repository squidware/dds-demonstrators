#ifndef SIGN_HPP
#define SIGN_HPP

enum Sign
{
    RED_CROSS,
    ARROW_RIGHT,
    ARROW_LEFT,
    BLANK,
    MAX_30,
    MAX_50,
    MAX_60,
    MAX_70,
    MAX_80,
    MAX_90,
};

#endif