#include "MatrixBoard.hpp"
#include "MQTTCommunication.hpp"
#include "DDSCommunication.hpp"
#include "ZMQCommunication.hpp"
#include "Test.hpp"

typedef std::chrono::high_resolution_clock Clock;

/**
 * @brief  Main function for the matrix board application
 *  Gets needed parameters and starts matrix board accordingly
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */

void ddsParameterError();
void zmqParameterError();
void mqttParameterError();

int main(int argc, char const *argv[])
{
    if(argc <= 1){
        std::cout << "more argmunents needed" << std::endl;
        return 1;
    }
    Test tester("startupstart");
    Clock::time_point start_time = Clock::now();
    std::string start_time_string = tester.getMiliFromTime(start_time);
    std::thread write_measurements_thread(&Test::WriteToFile, &tester, " ; ; ;startupstart;"+start_time_string, "starttime_.csv");
    write_measurements_thread.detach();
    int retval = 0;
    std::string protocol = argv[1];
    std::cout << protocol << std::endl;
    if(protocol == "MQTT"){
        if(argc != 4){
            mqttParameterError();
            return 1;
        }
        MQTTCommunication mqttobject(argv[2], argv[3], std::stoi(argv[4]));
        std::cout << "start matrixboard:" << argv[2] << std::endl;
        MatrixBoard matrixboard(&mqttobject, std::stoi(argv[2]));
        retval = matrixboard.run();
    }
    else if(protocol == "ZMQ"){
        if(argc != 5){
            zmqParameterError();
            return 1;
        }
        ZMQCommunication zmqobject(argv[2], argv[3], argv[4], argv[5]);
        std::cout << "start matrixboard:" << argv[2] << std::endl;
        MatrixBoard matrixboard(&zmqobject, std::stoi(argv[2]));
        retval = matrixboard.run();
    }
    else if(protocol == "DDS"){
        if(argc != 3){
            ddsParameterError();
            return 1;
        }
        DDSCommunication ddsobject(std::stoi(argv[2]));
        std::cout << "start matrixboard:" << argv[2] << std::endl;
        MatrixBoard matrixboard(&ddsobject, std::stoi(argv[2]));
        retval = matrixboard.run();
    }
    else{
        std::cout << "Not a known protocol" << std::endl;
        return 1;
    }
    if(retval != 0){
        std::cout << "The application was unable to" << 
        " start, please run as super user by using sudo" <<
        " or run from root terminal using su"<< std::endl;
    }
    return 0;
}

void ddsParameterError(){
        std::cerr
        << "Wrong input for this application for using DDS!\n\n"
        << "Format: ./rt_matrixboard DDS <Matrixboard ID>.\n\n"
        << "<Matrixboard ID>: The ID of the matrixboard within the network, must be unique, an integer, > 0. \n"
        ;
}

void zmqParameterError(){
        std::cerr
        << "Wrong input for this application for using ZMQ!\n\n"
        << "Format: ./rt_matrixboard ZMQ <Matrixboard ID> <Server sub address> <Server pub address> <Own address>\n\n"
        << "<Matrixboard ID>: The ID of the matrixboard within the network, must be unique, an integer, > 0. \n"
        << "<Server sub address>: The address of the running proxy to subscribe to, for example: tcp://192.168.178.32:5550. \n"
        << "<Server pub address>: The address of the running proxy to publish to, for example: tcp://192.168.178.32:5551. \n"
        << "<Own address>: The address of this device and port that this application uses, for example: tcp://192.168.178.30:5550. \n"
        ;
}

void mqttParameterError(){
        std::cerr
        << "Wrong input for this application for using MQTT!\n\n"
        << "Format: ./rt_matrixboard MQTT <Matrixboard ID> <Broker address> <QoS>\n\n"
        << "<Matrixboard ID>: The ID of the matrixboard within the network, must be unique, an integer, > 0 \n"
        << "<Broker address>: The address of the running MQTT broker to connect to, for example: tcp://192.168.178.32:1883. \n"
        << "<QoS>: The QoS level MQTT should use, either 0, 1 or 2. \n"
        ;
}