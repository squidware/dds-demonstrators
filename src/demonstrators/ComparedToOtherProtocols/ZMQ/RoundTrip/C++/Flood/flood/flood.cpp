#include <chrono>
#include <string>
#include <thread>

#include <iostream>

#include "flood.hpp"

/**
 * @brief Constructs a new flood object
 *
 * @param id the ID of the device
 * @param totalDevices the total number of devices in the flood loop
 * @param floodMessages the number of messages that will be send / received
 */
Flood::Flood(const unsigned int id, const unsigned int totalDevices, const unsigned int floodMessages, const std::string pubAddress, const std::string subAddress)
    : _id{id}, _totalDevices{totalDevices}, _totalFloodMsg{floodMessages}, _running{false}, _floodSend{0}, _floodReceived{0},
    _subAddress{subAddress}, _pubAddress{pubAddress},
    _writeTopic{_id == _totalDevices ? "flood1" : "flood" + std::to_string(_id + 1)}, _readTopic{"flood" + std::to_string(_id)},
    _context{}, _subSocket{_context, ZMQ_SUB}, _pubSocket{_context, ZMQ_PUB}
     {
    initialise();
}

/**
 * @brief Destroys the Flood object including allocated memory
 *
 */
Flood::~Flood() {

}

/**
 * @brief initialises the flood and the correct settings for DDS
 *
 */
void Flood::initialise() {
    _subSocket.connect(_subAddress);
    _pubSocket.bind(_pubAddress);
    _subSocket.set(zmq::sockopt::subscribe, _readTopic); 
    std::cout << "\n" << _id << " sub to:" << _subAddress << " pub to:" << _pubAddress << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(2));
}

/**
 * @brief runs the flood implementation
 *
 * @note this function could wait infinitely!
 */
void Flood::run() {
    _running = true;
    _timer.start();
    if (_id == 1) {
        runMaster();
    } else {
        runSlave();
    }
    _timer.stop();
}

/**
 * @brief runs the flood loop of the master
 *
 */
void Flood::runMaster() {
    zmq::message_t msg, topic;
    zmq::recv_result_t c1, c2;
    zmq::pollitem_t items[] = {{_subSocket, 0, ZMQ_POLLIN, 0}};
    // Initiate the flood
    while (_running) {
        zmq::poll(&items[0], 1, 0);  
        if(items[0].revents & ZMQ_POLLIN) {
            c1 = _subSocket.recv(topic, zmq::recv_flags::none);  
            c2 = _subSocket.recv(msg, zmq::recv_flags::none);
            if (_totalFloodMsg == ++_floodReceived) {
                _running = false;
            }   
        }
        if (_totalFloodMsg != _floodSend) {
            _floodSend++;
            _pubSocket.send(generateMessageFromString(_writeTopic), zmq::send_flags::sndmore);
            _pubSocket.send(generateMessageFromString(std::to_string(_id)), zmq::send_flags::none);
        }
    }
}

/**
 * @brief runs the flood loop of the slave
 *
 */
void Flood::runSlave() {
    zmq::message_t msg, topic;
    zmq::recv_result_t c1, c2;
    // Initiate the flood
    while (_running) {
        c1 = _subSocket.recv(topic, zmq::recv_flags::none);  
        c2 = _subSocket.recv(msg, zmq::recv_flags::none);   
        _floodReceived++;
        _pubSocket.send(generateMessageFromString(_writeTopic), zmq::send_flags::sndmore);
        _pubSocket.send(generateMessageFromString(std::to_string(_id)), zmq::send_flags::none);
        if (_totalFloodMsg == _floodReceived) {
            _running = false;
        }
        
    }
}

zmq::message_t Flood::generateMessageFromString(std::string message) {
    zmq::message_t msg(message.size());
    memcpy(msg.data(), message.c_str(), message.size());
    return msg;
}