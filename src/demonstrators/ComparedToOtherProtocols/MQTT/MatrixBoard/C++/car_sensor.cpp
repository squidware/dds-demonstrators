#include "car_sensor.hpp"
#include <random>

namespace matrixboard {

/**
 * @brief countedCars simulates a car counter sensor by returning a value for how much cars are counted
 *
 * @return unsigned int the amount of cars counted/simulated
 * @note The countedCars function returns a random number between minimalCarAmount and maximumCarAmount in the settings
 */
unsigned int CarSensor::countedCars() {
    std::random_device seed;        // Will be used to obtain a seed for the random number engine
    std::mt19937 generator(seed()); // Standard mersenne_twister_engine seeded with seed()
    std::uniform_int_distribution<> distribution(_settings.minimalCarAmount, _settings.maximumCarAmount);
    return distribution(generator);
}

/**
 * @brief changeSettings changes the settings of the car sensor
 *
 * @param settings the settings of the car sensor
 */
void CarSensor::changeSettings(const Settings settings) {
    if (settings.maximumCarAmount > settings.minimalCarAmount) {
        // Maximum car amount should always be higher compared to the minimum car amount
        _settings = settings;
    }
}

} // namespace matrixboard