#include "measurements_file.hpp"

/**
 * @brief MeasurementsFile sets the filename
 *
 * @param fileName the name of the measurements file
 */
MeasurementsFile::MeasurementsFile(const std::string fileName) : _fileName{fileName} {}

void MeasurementsFile::createFile(std::vector<std::string> column_names) {
    std::string columns = vectorToCommaSeperatedString(column_names);
    std::fstream measurementsFile(_fileName, std::fstream::in | std::fstream::out | std::fstream::app);
    measurementsFile << columns << "\n";
    measurementsFile.close();
}
/**
 * @brief Writes measurements to csv file
 * 
 * @param measurements vector of strings consisting of measurements in order complying with measurementsfile
 */

void MeasurementsFile::write(std::vector<std::string> measurements) {
    std::string measurement = vectorToCommaSeperatedString(measurements);
    std::fstream measurementsFile(_fileName, std::fstream::in | std::fstream::out | std::fstream::app);
    measurementsFile << measurement << "\n";
    measurementsFile.close();
}
/**
 * @brief Turns vector of strings in ; seperated string
 * 
 * @param vector vector that should be turned into a ; seperated string
 * @return std::string 
 */
std::string MeasurementsFile::vectorToCommaSeperatedString(std::vector<std::string> vector) {
    std::string csvstr = "";
    for(int i = 0; i < vector.size(); i++){
        csvstr += vector[i]+";";
    }
    csvstr.pop_back();  
    return csvstr;
}