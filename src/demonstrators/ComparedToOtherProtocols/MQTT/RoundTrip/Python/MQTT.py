#!/usr/bin/env python3

import paho.mqtt.client as mqtt
from datetime import datetime
from time import sleep
import sys

import logging
logging.basicConfig(level=logging.INFO)
from logging import getLogger
logger = getLogger(__name__)

ReveiceTopic = sys.argv[1] if len(sys.argv) >1 else 'hub'
SendTopic = sys.argv[2] if len(sys.argv) >2 else 'master'
SERVER = sys.argv[3] if len(sys.argv) >3 else 'localhost'
ISMASTER = int(sys.argv[4]) if len(sys.argv) >4 else 0
QOS = int(sys.argv[5]) if len(sys.argv) >5 else 0

if(ISMASTER):
    logger.info("the master is started with server:")
else:
    logger.info("a hub is started with server:")    
logger.info(SERVER)
logger.info(QOS)

EVERY=30


t0, t1 = None, None




def send_message(client, counter):
    client.publish(SendTopic, counter, QOS)
    logger.debug("ping client: %s, counter=%i", client, counter)
    logger.debug("sending on topic: %s " % SendTopic)

def on_data_received(client, userdata, message):
    count = int(message.payload.decode().split()[0])
    logger.debug("pong  %i", count)
    if(ISMASTER):
        send_message(client, count+1)
        if count % EVERY == 0:
            report(count)
    else:
        send_message(client, count)

def on_connect(client, userdata, flags, rc):
    global t0
    t0 = datetime.now()
    client.subscribe(ReveiceTopic, QOS)
    logger.debug("Subscribed to %s", ReveiceTopic)
    if(ISMASTER):
        logger.debug("sending first message")
        send_message(client, 1) # start counting
    logger.info("Started")

def report(c):
    global t1
    t1 = datetime.now()
    t = (t1-t0).total_seconds()
    print("PingPong already %s messages, in %2.1f seconds: %i msg/sec [%3.3f ms/msg]" % (c, t, c/t, t*1000/c))


def stop(c):
    global t1
    t1 = datetime.now()
    client.loop_stop()
    print("Stopped at:", t1)
    t = (t1-t0 ).total_seconds()
    print("PingPong %s messages, in %f seconds: %f msg/sec" % (c, t, c/t))
    quit()

client = mqtt.Client()
client.on_connect = on_connect # with 1st ping
client.on_message = on_data_received


client.connect(SERVER)
client.loop_start() # in thread

sleep(60*60*24)
client.loop_stop()



