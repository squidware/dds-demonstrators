#ifndef ROUNDTRIP_H
#define ROUNDTRIP_H

#include "performance/performance_measurements.hpp"

#include "mqtt/client.h"

/**
 * @brief RoundTrip contains the implementation of the roundtrip
 *
 */
class RoundTrip {
  public:
    RoundTrip(const unsigned int id, const unsigned int totalDevices, const unsigned int roundTrips, const std::string server_address, const int qos);
    ~RoundTrip() = default;

    int getTime() const { return _timer.getTime<std::chrono::microseconds>(); };

    void run();

  private: 
    const unsigned int _id;
    const unsigned int _totalDevices;
    unsigned int _totalRoundtrips;
    
    bool _message;
    bool _running;
    unsigned int _roundTrips;

    mqtt::async_client _client;
    
    mqtt::topic _topicRead, _topicWrite;
      
    PerformanceMeasurements _timer;

    void initialise();
};

#endif // ROUNDTRIP_H