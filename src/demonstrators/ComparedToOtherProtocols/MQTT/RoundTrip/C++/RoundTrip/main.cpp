#include <chrono>
#include <iostream>
#include <thread>
#include <fstream>
#include <string>

#include "roundtrip.hpp"

void errorMessage();
void WriteToFile(std::string line, std::string file_name);

/**
 * @brief main contains the main code for the round trip project
 *
 * @param argc number of arguments
 * @param argv the arguments of the function
 * @return int endstate of the application
 */
int main(int argc, char const *argv[]) {
    if (argc != 5) {
        errorMessage();
        return -1;
    }
    std::string server_address = "tcp://localhost:1883";
    int qos;
    unsigned int id;
    unsigned int totalDevices;
    unsigned int totalRoundtrips;
    try {
        id = std::stoul(argv[1]);
        totalDevices = std::stoul(argv[2]);
        totalRoundtrips = std::stoul(argv[3]);
        qos = std::stoi(argv[4]);
        
        if (id > totalDevices || totalDevices <= 1) {
            errorMessage();
            return -1;
        }
    } catch (const std::exception &e) {
        errorMessage();
        return -1;
    }
    RoundTrip roundtrip(id, totalDevices, totalRoundtrips, server_address, qos);
    roundtrip.run();

    const auto totalRoundtripTime = roundtrip.getTime();

    if (id != 1) {
        std::cout << "Roundtrip finished, see device 1 for measurements\n\n";
        return 0;
    }
    double timeBetweenDevices, avarageRoundtripTime;
    avarageRoundtripTime = totalRoundtripTime / static_cast<double>(totalRoundtrips);
    timeBetweenDevices= (totalRoundtripTime / static_cast<double>(totalRoundtrips)) / totalDevices;
    std::cout << "RoundTrip finished!\n"
              << "Total execution time for " << totalRoundtrips << " round trips: " << totalRoundtripTime
              << " microseconds\n"
              << "Average time per round trip: " << avarageRoundtripTime
              << " microseconds\n"
              << "Time between each device within the round trip: "
              << timeBetweenDevices << " microseconds\n\n";
    
    WriteToFile((totalDevices+";"+std::to_string(totalRoundtrips)+";"+std::to_string(qos)+";"+
    std::to_string(totalRoundtripTime)+";"+std::to_string(avarageRoundtripTime)+";"+
    std::to_string(timeBetweenDevices)), "roundtrip.csv");     

    return 0;
}

/**
 * @brief errorMessage contains the fault message for wrong parameters
 *
 */
void errorMessage() {
    std::cerr
        << "Wrong input for this application!\n\n"
        << "Format: ./RoundTrip <device ID> <Total devices> <Round trips> <QoS>\n\n"
        << "<device ID>: The ID of the device within the round trip, must be unique, > 0 and <= total devices\n"
        << "<Total devices>: The amount of devices in the round trip loop (the amount of round trip programs running)\n"
        << "<Round Trips>: Amount of round trips to execute\n"
        << "<QoS>: QoS MQTT should use\n\n"
        << "Note: Only the <device ID> is unique! The other 2 parameters MUST be equal for each round trip program!\n"
        << "Note: Device ID 1 initiates the round trip. Therefore, device ID 1 must be started last!\n"
        << "Note: <Round Trips> should be equal for all applications\n"
        ;
}

void WriteToFile(std::string line, std::string file_name){
    std::ofstream measurements_file(file_name, std::ios::app);
    measurements_file << line << "\n";
    measurements_file.close();
}
