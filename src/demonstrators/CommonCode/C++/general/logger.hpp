#ifndef SIMPLE_LOGGER_H
#define SIMPLE_LOGGER_H

#include <iostream>
#include <sstream>
#include <string>

#include "helper_functions.hpp"

/**
 * @brief Contains a simple implementation of a logger
 *
 * This class should be used without an object. This logger has configurable colors
 * and status texts, as well as 3 predefined ones. The color codes are not supported
 * by every terminal, some terminals could contain weird characters for this reason.
 */
class Logger {
  public:
    enum class StatusColor : uint8_t {
        BLACK = 30,
        RED = 31,
        GREEN = 32,
        YELLOW = 33,
        BLUE = 34,
        MAGENTA = 35,
        CYAN = 36,
        WHITE = 37,
        DEFAULT = 39,
    };
    enum class LogLevel { INFO, WARNING, ERROR };

    template <typename... Args>
    static void print(const StatusColor statusColor, const std::string &statusText, const Args &... logText);

    template <typename... Args>
    static void print(const LogLevel loglevel, const Args &... logText);

    static std::string currentTimeStamp();
    static std::string currentTimeStampAccurate();

  protected:
    template <typename... Args>
    static void printText(const Args &... logText);

    static std::string colorCode(const StatusColor color);

    static int getMilliseconds();
    static int getMicroseconds();
};

/**
 * @brief prints text with a customizable status to the terminal
 *
 * @param statusColor the color of the status
 * @param statusText the text of the status
 * @param logText the message that is printed to the terminal
 */
template <typename... Args>
void Logger::print(const StatusColor statusColor, const std::string &statusText, const Args &... logText) {
    printText(colorCode(statusColor), statusText + ": ", colorCode(StatusColor::DEFAULT), logText...);
}

/**
 * @brief prints text with a predefined status to the terminal
 *
 * @param loglevel the loglevel of the message, can be INFO, WARNING or ERROR
 * @param logText the text that is printed to the terminal
 */
template <typename... Args>
void Logger::print(const LogLevel loglevel, const Args &... logText) {
    switch (loglevel) {
    case LogLevel::INFO:
        printText(colorCode(StatusColor::GREEN), "INFO: ", colorCode(StatusColor::DEFAULT), logText...);
        break;
    case LogLevel::WARNING:
        printText(colorCode(StatusColor::YELLOW), "WARNING: ", colorCode(StatusColor::DEFAULT), logText...);
        break;
    case LogLevel::ERROR:
        printText(colorCode(StatusColor::RED), "ERROR: ", colorCode(StatusColor::DEFAULT), logText...);
        break;
    }
}

/**
 * @brief printText prints the text to the terminal using cout
 *
 * @param logText the text that is printed to the terminal
 */
template <typename... Args>
void Logger::printText(const Args &... logText) {
    std::cout << "[" << currentTimeStamp() << "]: " << HelperFunctions::argToString(logText...) << std::endl;
}

/**
 * @brief Converts a StatusColor to the correct color code for a linux terminal in a string
 *
 * @param color the wanted color to be converted to a color code
 * @return std::string the color code that works on some/most linux terminals
 * @note the color code stays active until a new color code is executed!
 */
inline std::string Logger::colorCode(const StatusColor color) {
    return "\033[" + std::to_string(static_cast<std::underlying_type<StatusColor>::type>(color)) + "m";
}

#endif // SIMPLE_LOGGER_H