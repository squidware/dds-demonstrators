cmake_minimum_required(VERSION 3.5)

# Set name of the executable
set(PROJECT MatrixBoard)

# Define the project as a CPP project
project(${PROJECT} )

#[[
    Add the files to the project
]]
# Relative include path
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/
                     ${CMAKE_CURRENT_SOURCE_DIR}/../../CommonCode/C++/)

# Set the sources
set(SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/matrixboard/car_sensor.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/matrixboard/matrixboard_display.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/matrixboard/dds/dds_active_matrixboard.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/debugger/debug_topic_dds.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/debugger/debug_topic_manager.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/../../CommonCode/C++/general/logger.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/../../CommonCode/C++/performance/resource_usage.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/matrixboard/dds/specific_matrixboard.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/matrixboard/matrixboard_application.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/matrixboard/dds/active_matrixboard_manager.cpp)

# Set the main
set(MAIN ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp)

# Add the sources and main to the executable
add_executable(${PROJECT} ${MAIN} ${SOURCES})

set_property(TARGET ${PROJECT} PROPERTY CXX_STANDARD 17)

# Add warnings
target_compile_options(${PROJECT} PRIVATE   -pedantic -Wall -Wcast-align -Wcast-qual -Wdisabled-optimization 
                                            -Winit-self -Wmissing-declarations -Wmissing-include-dirs -Wredundant-decls 
                                            -Wshadow -Wsign-conversion -Wundef -Werror
                                            -Wempty-body -Wignored-qualifiers -Wmissing-field-initializers 
                                            -Wsign-compare -Wtype-limits  -Wuninitialized)

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    target_compile_options(${PROJECT} PRIVATE   -Wno-psabi)
endif()
#[[
    Installed libraries for this project
]]
### Threading library
find_package(Threads REQUIRED)

# Add the threading library to the project
set(DEPENDENCIES ${CMAKE_THREAD_LIBS_INIT})

### DDS Library (CycloneDDS)
find_package(CycloneDDS REQUIRED COMPONENTS idlc PATHS "${CMAKE_CURRENT_SOURCE_DIR}/")

# This is a convenience function, provided by the CycloneDDS package,
# that will supply a library target related the the given idl file.
# In short, it takes the idl file, generates the source files with
# the proper data types and compiles them into a library.
idlc_generate(MBCData_lib "MBCData.idl")

# Add the DDS library to the project
target_link_libraries(${PROJECT} MBCData_lib CycloneDDS::ddsc)

#[[
    Linking the libraries
]]
# Link libraries
target_link_libraries(${PROJECT} ${DEPENDENCIES})