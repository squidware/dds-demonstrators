#ifndef DEBUG_MESSAGE_TYPES_HPP
#define DEBUG_MESSAGE_TYPES_HPP

namespace Debug {
enum class MessageTypes : unsigned int {
    CONNECTING = 1,
    DEVICE_CONNECTED = 2,
    DISCONNECTING = 3,
    DEVICE_DISCONNECTED = 4,
};
}

#endif