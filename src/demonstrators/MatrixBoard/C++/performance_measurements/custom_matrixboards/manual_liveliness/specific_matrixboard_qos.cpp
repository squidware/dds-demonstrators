#include "matrixboard/dds/specific_matrixboard.hpp"

namespace matrixboard {

/**
 * @brief configures the qos policies for the own matrixboard object
 *
 * @param qos the qos object
 */
void SpecificMatrixBoard::configureQoS(dds_qos_t *qos) {
    dds_qset_reliability(qos, DDS_RELIABILITY_RELIABLE, DDS_SECS(10));
    dds_qset_liveliness(qos, DDS_LIVELINESS_MANUAL_BY_TOPIC, DDS_MSECS(2500));
}

} // namespace matrixboard