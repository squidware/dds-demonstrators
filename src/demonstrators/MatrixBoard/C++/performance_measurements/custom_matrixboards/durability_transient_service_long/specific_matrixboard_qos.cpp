#include "matrixboard/dds/specific_matrixboard.hpp"

namespace matrixboard {

/**
 * @brief configures the qos policies for the own matrixboard object
 *
 * @param qos the qos object
 */
void SpecificMatrixBoard::configureQoS(dds_qos_t *qos) {
     dds_qset_reliability(qos, DDS_RELIABILITY_RELIABLE, DDS_SECS(10));
    dds_qset_liveliness(qos, DDS_LIVELINESS_MANUAL_BY_PARTICIPANT, DDS_MSECS(2500));
    dds_qset_durability_service(qos, DDS_MSECS(4000), DDS_HISTORY_KEEP_ALL, 5, 25, 25, 25);
    dds_qset_durability(qos, DDS_DURABILITY_TRANSIENT);
}

} // namespace matrixboard