#include "debug_topic_processor.hpp"

#include <chrono>
#include <thread>

namespace Debug {

/**
 * @brief Constructs a new DebugTopicProcessor object
 *
 * @param participant the DDS participant that this class is going to use for the DDS communication
 * @param id the ID of the matrix board of which the performance measurements must be read
 */
DebugTopicProcessor::DebugTopicProcessor(dds_entity_t *participant, const unsigned int id)
    : _id{id}, _isConnecting{false}, _isDisconnecting{false}, _debugTopic{participant, "DebugTopic"} {
    reset();
}

/**
 * @brief initializes the debug topic
 *
 */
void DebugTopicProcessor::initialize() { _debugTopic.initialize(false, true); }

/**
 * @brief resets this class
 *
 */
void DebugTopicProcessor::reset() {
    _receivedConnected.first.clear();
    _receivedConnected.second.clear();
    resetDisconnected();
}

/**
 * @brief resets the disconnected results within this class
 *
 */
void DebugTopicProcessor::resetDisconnected() {
    _receivedDisconnected.first.clear();
    _receivedDisconnected.second.clear();
}

/**
 * @brief checks the debug topic for new messages
 * If there are new messages, these are being read and stored in the applicable buffer value
 *
 */
void DebugTopicProcessor::checkDebugTopic() {
    if (_debugTopic.checkNewData()) {
        DebugData_DebugInfo received = _debugTopic.getLastMessage();
        const MessageTypes messageType = static_cast<MessageTypes>(received.messageType);

        if (received.topicId != _id) {
            return;
        }

        if (messageType == MessageTypes::CONNECTING || messageType == MessageTypes::DEVICE_CONNECTED) {
            if (messageType == MessageTypes::CONNECTING) {
                _receivedConnected.first.push_back(received.datetime);
                Logger::print(Logger::StatusColor::YELLOW, "PERFORMANCE",
                              "Received connect time first: ", received.datetime);
            } else {
                _receivedConnected.second.push_back(received.datetime);
                Logger::print(Logger::StatusColor::YELLOW, "PERFORMANCE",
                              "Received connect time second: ", received.datetime);
            }
        }
        if (messageType == MessageTypes::DEVICE_DISCONNECTED) {
            Logger::print(Logger::StatusColor::YELLOW, "PERFORMANCE",
                          "Received disconnect time second: ", received.datetime);
            _receivedDisconnected.second.push_back(received.datetime);
        }
    }
    return;
}

/**
 * @brief sets the disconnected time (because this application should terminate the matrix board application and store
 * the time when it is terminated)
 *
 * @param datetime the timestamp of the disconnect time
 */
void DebugTopicProcessor::setDisconnected(const std::string &datetime) {
    _receivedDisconnected.first.push_back(datetime);
}

} // namespace Debug