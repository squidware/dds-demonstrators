#ifndef PERFORMANCE_MASTER_HPP
#define PERFORMANCE_MASTER_HPP

#include <string>
#include <unordered_map>

#include "dds/debug_topic_processor.hpp"
#include "file_manager.hpp"
#include "general/logger.hpp"
#include "general/measurements_buffer.hpp"
#include "general/timer.hpp"

/**
 * @brief executes the master for the performance measurements
 *
 * This class keeps track of the performance measurements and stores the results when the measurement is finished
 */
class Measurements {
  public:
    Measurements(FileManager *file, const unsigned int id);
    ~Measurements();

    void checkChanges();
    void setDisconnectTime(const std::string &datetime) { _debugTopic.setDisconnected(datetime); }

    inline void addCPU(const unsigned int pid, const double cpuUsage);
    inline void addPhysMemory(const unsigned int pid, const double memory);
    inline void addVirtMemory(const unsigned int pid, const double memory);

    void finishMeasurement(const unsigned int pidApplication);

    void startNewApplication(const unsigned int applicationPID);

    void resetDisconnect() { _debugTopic.resetDisconnected(); }

  private:
    struct ResourceBuffer {
        double totalUsage = 0;
        int measurements = 0;
    };

    struct MeasuredData {
        MeasurementsBuffer<int> connectTime;
        MeasurementsBuffer<int> disconnectTime;
        ResourceBuffer cpuUsage;
        ResourceBuffer physicalMemoryUsage;
        ResourceBuffer virtualMemoryUsage;
    };

    Debug::DebugTopicProcessor _debugTopic;
    FileManager *_file;
    bool _timerStarted;
    const unsigned int _id;
    std::unordered_map<unsigned int, MeasuredData> _measurements;
    dds_entity_t _participant;

    Timer<int64_t, std::milli> _timer;

    void readMeasurements(const unsigned int applicationPID);
    void processConnectData(const unsigned int applicationPID, const Debug::DebugTopicProcessor::ReceivedMsg &message);
    void processDisconnectData(const unsigned int applicationPID,
                               const Debug::DebugTopicProcessor::ReceivedMsg &message);
};

/**
 * @brief adds a CPU measurement to the buffer
 *
 * @param pid the pid of the application
 * @param cpuUsage the CPU usage that is measured
 */
inline void Measurements::addCPU(const unsigned int pid, const double cpuUsage) {
    auto parameter = _measurements.find(pid);
    if (parameter == _measurements.end()) {
        Logger::print(Logger::LogLevel::ERROR, "CPU: Couldn't find application with PID: ", pid);
        return;
    }
    parameter->second.cpuUsage.totalUsage += cpuUsage;
    parameter->second.cpuUsage.measurements++;
}

/**
 * @brief adds a physical memory usage measurement to the buffer
 *
 * @param pid the pid of the application
 * @param cpuUsage the physical memory usage that is measured
 */
inline void Measurements::addPhysMemory(const unsigned int pid, const double memory) {
    auto parameter = _measurements.find(pid);
    if (parameter == _measurements.end()) {
        Logger::print(Logger::LogLevel::ERROR, "PhysMemory: Couldn't find application with PID: ", pid);
        return;
    }
    parameter->second.physicalMemoryUsage.totalUsage += memory;
    parameter->second.physicalMemoryUsage.measurements++;
}

/**
 * @brief adds a virtual memory usage measurement to the buffer
 *
 * @param pid the pid of the application
 * @param cpuUsage the virtual memory usage usage that is measured
 */
inline void Measurements::addVirtMemory(const unsigned int pid, const double memory) {
    auto parameter = _measurements.find(pid);
    if (parameter == _measurements.end()) {
        Logger::print(Logger::LogLevel::ERROR, "VirtMemory: Couldn't find application with PID: ", pid);
        return;
    }
    parameter->second.virtualMemoryUsage.totalUsage += memory;
    parameter->second.virtualMemoryUsage.measurements++;
}

#endif