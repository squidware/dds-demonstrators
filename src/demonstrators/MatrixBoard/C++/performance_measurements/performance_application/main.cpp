#include <cmath> // for pow()

#include "tests_implementation.hpp"

void errorMessage(const std::string &applicationName, const std::string &masterName, const std::string &slaveName);
void errorMessageMaster();
void errorMessageSlave();
std::string getApplicationName(const std::string &pathName, const std::string &masterName,
                               const std::string &slaveName);

/**
 * @brief main starts the matrixboard simulation
 *
 * @param argc arguments not used
 * @param argv arguments not used
 * @return int state of the code
 */
int main(int argc, const char **argv) {
    const std::string masterName{"Master"};
    const std::string slaveName{"Slave"};

    const std::string applicationName{getApplicationName(argv[0], masterName, slaveName)};
    if (argc < 2) {
        errorMessage(applicationName, masterName, slaveName);
        return -1;
    }
    std::string argument = argv[1];
    unsigned int id;
    std::string fileName = "logging.csv";
    std::string applicationsLocation = "../../custom_matrixboards/build/";
    std::string customApplicationName;
    int executeTests = 4;
    try {
        id = std::stoul(argument);
        if (id > std::numeric_limits<unsigned int>::max() || id == 0) {
            errorMessage(applicationName, masterName, slaveName);
            return -1;
        }
        if (argc >= 3) {
            fileName = argv[2];
        }
        if (argc >= 4) {
            executeTests = std::stoi(argv[3]);
        }
        if (argc >= 5) {
            applicationsLocation = argv[4];
        }
        if (argc >= 6) {
            customApplicationName = argv[5];
        }
    } catch (const std::exception &e) {
        errorMessage(applicationName, masterName, slaveName);
        return -1;
    }
    try {
        TestsImplementation tests(executeTests, fileName, applicationsLocation);
        if (customApplicationName.size() == 0) {
            tests.execute(id);
        } else {
            tests.execute(id, customApplicationName);
        }
    } catch (const std::exception &e) {
        Logger::print(Logger::LogLevel::ERROR, "Performance measurements crashed: ", e.what());
        Logger::print(Logger::LogLevel::ERROR, "Quitting program!");
        return -1;
    }
    Logger::print(Logger::StatusColor::YELLOW, "PERFORMANCE", "Measurements finished!");
    return 0;
}

/**
 * @brief Error message to print when incorrect parameters are given to the application
 *
 */
void errorMessage(const std::string &applicationName, const std::string &masterName, const std::string &slaveName) {
    if (applicationName == slaveName) {
        errorMessageSlave();
    } else if (applicationName == masterName) {
        errorMessageMaster();
    } else {
        std::cerr << "The application name is not a known name\n"
                  << "Please check if you changed the correct parameters";
    }
}

/**
 * @brief Error message to print when incorrect parameters are given to the slave application
 *
 */
void errorMessageSlave() {
    std::cerr << "Wrong input for this application!\n\n"
              << "Format: ./Slave <MatrixBoard ID> \n"
              << "<MatrixBoard ID>: The ID of the matrix board (may be 1 - 2^31)\n"
              << "Note: The <MatrixBoard ID> should be unique for every matrix board!\n"
              << "Note: this slave application should be started before the master application! \n";
}

/**
 * @brief Error message to print when incorrect parameters are given to the master application
 *
 */
void errorMessageMaster() {
    std::cerr << "Wrong input for this application!\n\n"
              << "Format: ./Master <MatrixBoard ID> <Name of logging file> <Execution amount> <Application Location> "
                 "<Application name>\n"
              << "<MatrixBoard ID>: The ID of the matrix board (may be 1 - 2^31)\n"
              << "<Name of logging file>: OPTIONAL: The name of the file where the performance results are stored\n"
              << "<Execution amount>: OPTIONAL: The amount of executions of the performance measurements\n"
              << "<Application location>: OPTIONAL: The location where all external applications are in (by default "
                 "\"../../custom_matrixboards/build/\"\n"
              << "<Application name>: OPTIONAL: can be configured to choose what custom configuration to execute. If "
                 "only one configuration measurement is necessary.\n\n"
              << "Note: The <MatrixBoard ID> should be unique for every matrix board!\n"
              << "Note: this is the master for the performance measurements, the slave should be started first\n";
}

/**
 * @brief reads the application name based on the path name, only checking for the master and slave name
 *
 * @return string the application name
 */
std::string getApplicationName(const std::string &pathName, const std::string &masterName,
                               const std::string &slaveName) {
    std::string applicationName;
    if (pathName.size() >= masterName.size() || pathName.size() >= slaveName.size()) {
        if (pathName.back() == masterName.back()) {
            applicationName = pathName.substr(pathName.size() - masterName.size(), pathName.size());
        } else if (pathName.back() == slaveName.back()) {
            applicationName = pathName.substr(pathName.size() - slaveName.size(), pathName.size());
        }
    }
    return applicationName;
}