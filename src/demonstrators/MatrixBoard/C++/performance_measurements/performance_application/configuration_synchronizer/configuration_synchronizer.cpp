#include "configuration_synchronizer.hpp"

#include "MBCData.h"

namespace Performance {

/**
 * @brief Construct a new StartApplication object
 *
 * @param participant the DDS participant that this class is going to use for the DDS communication
 */
ConfigurationSynchronizer::ConfigurationSynchronizer(dds_entity_t *participant)
    : _applicationTopic{participant, "ApplicationTopic"} {}

/**
 * @brief sends an application to the application topic
 *
 * @param applicationName the name of the application that is sent to the application topic
 */
void ConfigurationSynchronizer::send(const std::string &applicationName) {
    Performance_Application message;
    char buffer[100];
    applicationName.copy(buffer, 100, 0);
    buffer[applicationName.size()] = '\0';
    message.applicationName = buffer;
    _applicationTopic.write(message);
}

/**
 * @brief checks if new data is received on the application topic
 *
 * @return true new data is received
 * @return false no new data is received
 */
bool ConfigurationSynchronizer::receivedNewApplication() { return _applicationTopic.checkNewData(); }

/**
 * @brief returns the last received message from the application topic (the application name)
 *
 * @return std::string the application name that is received on the application topic
 */
std::string ConfigurationSynchronizer::getNewApplication() {
    Performance_Application receivedMessage = _applicationTopic.getLastMessage();
    return receivedMessage.applicationName;
}

} // namespace Performance