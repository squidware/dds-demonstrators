#ifndef APPLICATION_TOPIC_DDS_HPP
#define APPLICATION_TOPIC_DDS_HPP

#include "MBCData.h"
#include "dds/dds_basics.hpp"
#include <dds/dds.h>

#include <string>

namespace Performance {

/**
 * @brief connects and communicates with the application topic using DDS
 * 
 */
class ApplicationTopicDDS : public DDSBasics<Performance_Application> {
  public:
    ApplicationTopicDDS(dds_entity_t *participant, const std::string &topicName);
    ~ApplicationTopicDDS() = default;

    void initialize();

  private:
    const std::string _topicName;

    void configureQoS(dds_qos_t *qos);
};

} // namespace Performance
#endif