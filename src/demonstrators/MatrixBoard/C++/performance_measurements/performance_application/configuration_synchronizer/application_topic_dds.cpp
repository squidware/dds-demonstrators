#include "application_topic_dds.hpp"

#include <chrono>
#include <thread>

namespace Performance {

/**
 * @brief Constructs a new ApplicationTopicDDS object
 *
 * @param participant the DDS participant that this class is going to use for the DDS communication
 * @param topicName the name of the topic
 */
ApplicationTopicDDS::ApplicationTopicDDS(dds_entity_t *participant, const std::string &topicName)
    : DDSBasics<Performance_Application>{participant, Performance_Application_desc}, _topicName{topicName} {}

/**
 * @brief registers to the application topic
 *
 */
void ApplicationTopicDDS::initialize() { registerToTopic(_topicName); }

/**
 * @brief configures the qos policies for the application topic
 *
 * @param qos the qos object
 */
void ApplicationTopicDDS::configureQoS(dds_qos_t *qos) {
    dds_qset_reliability(qos, DDS_RELIABILITY_RELIABLE, DDS_SECS(10));
    dds_qset_history(qos, DDS_HISTORY_KEEP_ALL, 500);
}

} // namespace Performance