#include "tests_implementation.hpp"

#include <chrono>
#include <thread>

#include "general/helper_functions.hpp"
#include "general/timer.hpp"
#include "measurements/measurements.hpp"
#include "performance/resource_usage.hpp"
#include "run_matrixboard/run_application.hpp"

/**
 * @brief Construct a new TestsImplementation object
 *
 * @param numberOfTests the number of tests that should be executed
 * @param filename the file name where the results are stored
 * @param applicationsLocation the location where the custom applications are stored
 */
TestsImplementation::TestsImplementation(const int numberOfTests, const std::string &filename,
                                         const std::string &applicationsLocation)
    : _numberOfTests{numberOfTests}, _applicationsLocation{applicationsLocation},
      _applicationTopic{&_participant}, _file{filename} {
    _participant = dds_create_participant(DDS_DOMAIN_DEFAULT, NULL, NULL);
    _applicationTopic.initialize();
}

/**
 * @brief Destroy the TestsImplementation object
 *
 */
TestsImplementation::~TestsImplementation() { dds_delete(_participant); }

/**
 * @brief executes the master containing only one custom configuration
 *
 * @param matrixboardID the ID of the matrix board application
 * @param applicationName the name of the custom configuration that needs to be executed
 */
void TestsImplementation::execute(const unsigned int matrixboardID, const std::string &applicationName) {
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    sendAndExecuteApplication(matrixboardID, applicationName);

    _applicationTopic.send("STOP");
}

/**
 * @brief executes the master containing all custom configurations
 *
 * @param matrixboardID the ID of the matrix board application that needs to be started
 */
void TestsImplementation::execute(const unsigned int matrixboardID) {
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    sendAndExecuteApplication(matrixboardID, "default");

    sendAndExecuteApplication(matrixboardID, "durability_persistent");

    sendAndExecuteApplication(matrixboardID, "durability_transient");

    sendAndExecuteApplication(matrixboardID, "durability_transient_local");

    sendAndExecuteApplication(matrixboardID, "durability_transient_service_long");

    sendAndExecuteApplication(matrixboardID, "durability_transient_service_short");

    sendAndExecuteApplication(matrixboardID, "durability_volatile");

    sendAndExecuteApplication(matrixboardID, "long_deadline");

    sendAndExecuteApplication(matrixboardID, "long_lifespan");

    sendAndExecuteApplication(matrixboardID, "long_liveliness");

    sendAndExecuteApplication(matrixboardID, "manual_liveliness");

    sendAndExecuteApplication(matrixboardID, "reliable");

    sendAndExecuteApplication(matrixboardID, "short_deadline");

    sendAndExecuteApplication(matrixboardID, "short_lifespan");

    sendAndExecuteApplication(matrixboardID, "short_liveliness");

    sendAndExecuteApplication(matrixboardID, "unreliable");

    _applicationTopic.send("STOP");
}

/**
 * @brief sends the application that needs to be executed to the slave (application topic) and executes the application
 *
 * @param matrixboardID the ID of the matrix board
 * @param applicationName the name of the matrix board application
 */
void TestsImplementation::sendAndExecuteApplication(const unsigned int matrixboardID,
                                                    const std::string &applicationName) {
    _applicationTopic.send(applicationName);
    executeApplicationLoop(matrixboardID, applicationName);
}

/**
 * @brief executeApplicationLoop stores the new measurement name in the logging file and starts
 * executing the matrix board application for an X times (configured in the constructor).
 *
 * @param matrixboardID the ID of the matrix board application
 * @param applicationName the name of the application that needs to be executed
 */
void TestsImplementation::executeApplicationLoop(const unsigned int matrixboardID, const std::string &applicationName) {
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    Logger::print(Logger::StatusColor::YELLOW, "PERFORMANCE", "Starting new application: ", applicationName);
    _file.storeLine(applicationName);
    for (int i = 0; i < _numberOfTests; i++) {
        executeApplication(matrixboardID, _applicationsLocation + applicationName);
    }
}

/**
 * @brief executes an application and measuring the CPU, physical and virtual memory usage.
 *
 * @param matrixboardID the ID of the matrix board that is started
 * @param applicationLocation the location of the matrix board application
 */
void TestsImplementation::executeApplication(const unsigned int matrixboardID, const std::string &applicationLocation) {
    Measurements performanceTracker(&_file, matrixboardID);

    int getDelay = HelperFunctions::getRandomTime(2000, 7000);
    Timer timer(std::chrono::milliseconds{getDelay});

    RunApplication application;
    // Start the matrix board application
    application.runMatrixBoard(applicationLocation, matrixboardID);
    const unsigned int applicationPID = application.getPID();
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    ResourceUsage cpu(applicationPID);
    timer.start(false);
    performanceTracker.startNewApplication(applicationPID);

    double cpuUsageVal;
    while (!timer.elapsed()) {
        cpuUsageVal = cpu.getCPUusage();
        if (cpuUsageVal != -1) {
            performanceTracker.addCPU(applicationPID, cpuUsageVal);
        }
        performanceTracker.addPhysMemory(applicationPID, ResourceUsage::getPhysicalMemoryUsage(applicationPID));
        performanceTracker.addVirtMemory(applicationPID, ResourceUsage::getVirtualMemoryUsage(applicationPID));

        performanceTracker.checkChanges();
        std::this_thread::sleep_for(std::chrono::milliseconds{10}); // Sleep for less CPU usage
    }
    performanceTracker.resetDisconnect();
    application.stopMatrixBoard();
    performanceTracker.setDisconnectTime(Logger::currentTimeStampAccurate());
    timer.changeTime(std::chrono::milliseconds{6000});
    timer.restart();
    while (!timer.elapsed()) {
        performanceTracker.checkChanges();
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    performanceTracker.finishMeasurement(applicationPID);
}