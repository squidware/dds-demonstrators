#ifndef TESTS_IMPLEMENTATION_HPP
#define TESTS_IMPLEMENTATION_HPP

#include <string>

#include "configuration_synchronizer/configuration_synchronizer.hpp"
#include "measurements/file_manager.hpp"

/**
 * @brief Class containing the implementation of a performance test
 *
 */
class TestsImplementation {
  public:
    TestsImplementation(const int numberOfTests, const std::string &filename, const std::string &applicationsLocation);
    ~TestsImplementation();

    void execute(const unsigned int matrixboardID);
    void execute(const unsigned int matrixboardID, const std::string &applicationName);

  private:
    const int _numberOfTests;
    const std::string _applicationsLocation;
    dds_entity_t _participant;

    Performance::ConfigurationSynchronizer _applicationTopic;
    FileManager _file;
    void sendAndExecuteApplication(const unsigned int matrixboardID, const std::string &applicationName);
    void executeApplicationLoop(const unsigned int matrixboardID, const std::string &applicationName = "default");
    void executeApplication(const unsigned int matrixboardID, const std::string &applicationLocation);
};

#endif