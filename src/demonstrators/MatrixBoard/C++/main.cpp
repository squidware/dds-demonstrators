#include <complex> //pow
#include <iostream>
#include <signal.h>
#include <string>

#include "general/logger.hpp"
#include "matrixboard/matrixboard_application.hpp"

void stop(int signal);
void errorMessage();

/**
 * @brief main starts the matrixboard simulation
 *
 * @param argc arguments not used
 * @param argv arguments not used
 * @return int state of the code
 */
int main(int argc, const char **argv) {
    if (argc != 2) {
        errorMessage();
        return -1;
    }
    std::string argument = argv[1];
    unsigned int id;
    try {
        id = std::stoul(argument);
        if (id > static_cast<unsigned>(std::pow(2, 31)) || id == 0) {
            errorMessage();
            return -1;
        }
    } catch (const std::exception &e) {
        errorMessage();
        return -1;
    }

    Logger::print(Logger::LogLevel::INFO, "Matrixboard application started!");
    try {
        signal(SIGINT, stop);
        MatrixBoardApplication board(id);
        board.run();
    } catch (const std::exception &e) {
        Logger::print(Logger::LogLevel::ERROR, "Program crashed: ", e.what());
        Logger::print(Logger::LogLevel::ERROR, "Quitting program!");
        return -1;
    }
    Logger::print(Logger::LogLevel::WARNING, "Matrixboard application stopped!");
    return 0;
}

/**
 * @brief Error message to print when incorrect parameters are given to the application
 *
 */
void errorMessage() {
    std::cerr << "Wrong input for this application!\n\n"
              << "Format: ./MatrixBoard <MatrixBoard ID>\n"
              << "<MatrixBoard ID>: The ID of the matrix board (may be 1 - 2^31)\n\n"
              << "Note: The <MatrixBoard ID> should be unique for every matrix board!\n";
}

/**
 * @brief Function to stop the application
 *
 * @param signal the stop signal
 */
void stop(int signal) { kill(getpid(), SIGTERM); }