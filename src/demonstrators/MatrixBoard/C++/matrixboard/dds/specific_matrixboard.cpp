#include "specific_matrixboard.hpp"

#include <chrono>
#include <thread>

#include "debugger/debug_topic_manager.hpp"

namespace matrixboard {

/**
 * @brief Constructs a new SpecificMatrixBoard object
 *
 * @param participant the DDS participant that this class is going to use for the DDS communication
 * @param id the ID of the matrix board
 * @param topic the topic of the SpecificMatrixBoard shared topic
 */
SpecificMatrixBoard::SpecificMatrixBoard(const std::string &generalTopicName, const MatrixboardInfo matrixboardInfo)
    : DDSBasics<MBCData_Msg>{&_ddsParticipant, MBCData_Msg_desc}, _topicName{generalTopicName +
                                                                             std::to_string(matrixboardInfo.topicId)},
      _matrixboardInfo{matrixboardInfo}, _livelinessArgs{&_matrixboardInfo, &_writer} {
    _ddsParticipant = dds_create_participant(DDS_DOMAIN_DEFAULT, NULL, NULL);
    if (_ddsParticipant < 0) {
        throw std::runtime_error(
            HelperFunctions::argToString("Matrixboard: ", __func__, ": ", dds_strretcode(-_ddsParticipant)));
    }
}

SpecificMatrixBoard::~SpecificMatrixBoard() { dds_delete(_ddsParticipant); }

/**
 * @brief initialises the specific matrix board object and registers to the topic
 *
 */
void SpecificMatrixBoard::initialize() {
    if (_matrixboardInfo.isWriter) {
        registerToTopic(_topicName, true, false);
    } else {
        registerToTopic(_topicName, false, true);
    }
}

/**
 * @brief registers to a topic
 *
 * @note This function should only be executed once!
 * @note this function overloads the function in DDSBasics!
 */
void SpecificMatrixBoard::registerToTopic(const std::string &topicName, const bool createWriter,
                                          const bool createReader) {
    _topic = dds_create_topic(*_participant, &_descriptor, topicName.c_str(), NULL, NULL);
    checkDDSerror(_topic);
    dds_listener_t *listener = createListener();
    dds_qos_t *qos = dds_create_qos();

    configureListener(listener);
    configureQoS(qos);

    if (createWriter) {
        _writer = dds_create_writer(*_participant, _topic, qos, listener);
        checkDDSerror(_writer);
    }
    if (createReader) {
        _reader = dds_create_reader(*_participant, _topic, qos, listener);
        checkDDSerror(_reader);
    }
    if (!_matrixboardInfo.isWriter) {
        Debug::DebugTopicManager::sendConnectTime(_matrixboardInfo.topicId);
    }
    dds_delete_qos(qos);
    dds_delete_listener(listener);
    std::this_thread::sleep_for(std::chrono::milliseconds(_ddsInitialization));
}

/**
 * @brief creates a custom listener for adding the matrix board ID and isWriter information
 *
 * @return dds_listener_t* the created listener
 */
dds_listener_t *SpecificMatrixBoard::createListener() { return dds_create_listener(&_livelinessArgs); }

/**
 * @brief configures the qos policies for the own matrixboard object
 *
 * @param qos the qos object
 */
void SpecificMatrixBoard::configureQoS(dds_qos_t *qos) {
    dds_qset_reliability(qos, DDS_RELIABILITY_RELIABLE, DDS_SECS(10));
    dds_qset_liveliness(qos, DDS_LIVELINESS_MANUAL_BY_PARTICIPANT, DDS_MSECS(2500));
}

/**
 * @brief contains the configuration of the listener
 *
 * @param listener the listener object
 */
void SpecificMatrixBoard::configureListener(dds_listener_t *listener) {
    if (_matrixboardInfo.isWriter) {
        dds_lset_publication_matched(listener, livelinessCallbackWriter);
    } else {
        dds_lset_liveliness_changed(listener, livelinessCallbackReader);
    }
}

/**
 * @brief this contains the liveliness callback
 * @note the first time this call back is executed is itself joining the topic. That one should be ignored
 */
void SpecificMatrixBoard::livelinessCallbackReader(dds_entity_t reader, dds_liveliness_changed_status status,
                                                   void *arg) {
    const std::string timestamp{Logger::currentTimeStampAccurate()};
    LivelinessArguments *args = static_cast<LivelinessArguments *>(arg);

    if (status.alive_count_change < 0 && !args->matrixboardInfo->isWriter) {
        Debug::DebugTopicManager::sendDeviceDisconnected(args->matrixboardInfo->topicId, timestamp);
    }
    if (status.alive_count_change > 0 && args->matrixboardInfo->isWriter) {
        Debug::DebugTopicManager::sendDeviceRegistered(args->matrixboardInfo->topicId, timestamp);
    }
}

/**
 * @brief this contains the liveliness callback of the writer
 * This callback is executed when a reader gets added or removed from the topic
 */
void SpecificMatrixBoard::livelinessCallbackWriter(dds_entity_t writer, dds_publication_matched_status_t status,
                                                   void *arg) {
    const std::string timestamp{Logger::currentTimeStampAccurate()};
    LivelinessArguments *args = static_cast<LivelinessArguments *>(arg);

    if (status.current_count_change < 0) {
        Debug::DebugTopicManager::sendDeviceDisconnected(args->matrixboardInfo->topicId, timestamp);
    }
    if (status.current_count_change > 0) {
        Debug::DebugTopicManager::sendDeviceRegistered(args->matrixboardInfo->topicId, timestamp);
    }
}

} // namespace matrixboard