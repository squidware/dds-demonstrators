#include "matrixboard_application.hpp"

#include <chrono>
#include <dds/dds.h>
#include <thread>
#include <vector>

#include "debugger/debug_topic_manager.hpp"
#include "general/helper_functions.hpp"
#include "general/logger.hpp"
#include "general/timer.hpp"

std::atomic_bool MatrixBoardApplication::_stop;

/**
 * @brief Construct a new Matrix Board object
 *
 * @param id the unique matrix board ID
 */
MatrixBoardApplication::MatrixBoardApplication(const unsigned int id)
    : _id{id}, _oldTraffic{0}, _activeMB{&_participant, id, "Matrixboards"},
      _ownMatrixboard{"matrixboard", matrixboard::SpecificMatrixBoard::MatrixboardInfo(id, true)} {
    createDDSparticipant();
}

/**
 * @brief Destroy the Matrix Board:: Matrix Board object
 *
 */
MatrixBoardApplication::~MatrixBoardApplication() { removeDDSparticipant(); }

/**
 * @brief createDDSparticipant creates a DDS participant
 *
 */
void MatrixBoardApplication::createDDSparticipant() {
    _participant = dds_create_participant(DDS_DOMAIN_DEFAULT, NULL, NULL);
    if (_participant < 0) {
        throw std::runtime_error(
            HelperFunctions::argToString("Matrixboard: ", __func__, ": ", dds_strretcode(-_participant)));
    }
    Debug::DebugTopicManager::initialize(&_participant); // Initialize the debugger
}

/**
 * @brief removeDDSparticipant removes a DDS participant
 *
 */
void MatrixBoardApplication::removeDDSparticipant() {
    dds_return_t rc_read = dds_delete(_participant);
    if (rc_read != DDS_RETCODE_OK) {
        throw std::runtime_error(
            HelperFunctions::argToString("Matrixboard: ", __func__, ": ", dds_strretcode(-rc_read)));
    }
}

/**
 * @brief Runs the matrix board code
 *
 */
void MatrixBoardApplication::run() {
    _ownMatrixboard.initialize();
    _activeMB.initialize();

    MBCData_Msg msg{createMessage("MBC_" + std::to_string(_id))};
    Timer timerNewSensorData(std::chrono::milliseconds{_trafficTime});

    unsigned int traffic1, traffic2;

    timerNewSensorData.start(false);
    updateSubsequentBoards();

    while (!_stop) {
        if (timerNewSensorData.elapsed()) {
            msg.traffic = static_cast<int32_t>(sensor_.countedCars());
            _ownMatrixboard.write(msg);
            display_.setDisplayValue(calculateSpeed((traffic1 + traffic2) / 2));

            timerNewSensorData.restart();
        }
        if (_activeMB.checkChanges()) {
            updateSubsequentBoards();
        }
        updateTraffic(&traffic1, &traffic2);
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
    }
}

/**
 * @brief Updates the subsequent and the matrix board after the subsequent matrix board as an object
 * This function updates the objects that are stored in the unique_ptr's.
 * This function should be executed when the ID's of the subsequent matrix
 * boards has been updated.
 */
void MatrixBoardApplication::updateSubsequentBoards() {
    _subsequentBoardIDs = _activeMB.getNextBoards();

    if (_subsequentBoardIDs.subsequent != 0) {
        if (_subsequentBoards.find(_subsequentBoardIDs.subsequent) == _subsequentBoards.end()) {
            _subsequentBoards.insert(std::make_pair(
                _subsequentBoardIDs.subsequent, std::make_unique<matrixboard::SpecificMatrixBoard>(
                                                    "matrixboard", matrixboard::SpecificMatrixBoard::MatrixboardInfo(
                                                                       _subsequentBoardIDs.subsequent, false))));
            _subsequentBoards.find(_subsequentBoardIDs.subsequent)->second->initialize();
            Logger::print(Logger::StatusColor::CYAN, "CONNECTED",
                          "Subsequent matrix board: ", _subsequentBoardIDs.subsequent);
        }
    } else {
        Logger::print(Logger::StatusColor::BLUE, "DISCONNECTED", "Subsequent matrix board disconnected!");
    }
    if (_subsequentBoardIDs.afterSubsequent != 0) {
        if (_subsequentBoards.find(_subsequentBoardIDs.afterSubsequent) == _subsequentBoards.end()) {
            _subsequentBoards.insert(std::make_pair(
                _subsequentBoardIDs.afterSubsequent,
                std::make_unique<matrixboard::SpecificMatrixBoard>(
                    "matrixboard",
                    matrixboard::SpecificMatrixBoard::MatrixboardInfo(_subsequentBoardIDs.afterSubsequent, false))));
            _subsequentBoards.find(_subsequentBoardIDs.afterSubsequent)->second->initialize();
            Logger::print(Logger::StatusColor::CYAN, "CONNECTED",
                          "After the subsequent matrix board: ", _subsequentBoardIDs.afterSubsequent);
        }
    } else {
        Logger::print(Logger::StatusColor::BLUE, "DISCONNECTED", "After the subsequent matrix board disconnected!");
    }
    removeOldBoards();
}

/**
 * @brief Reads from the subsequent matrix boards and updates the traffic
 *
 * @param traffic1 [OUT] the traffic that was measured from the subsequent matrix board
 * @param traffic2 [OUT] the traffic that was measured from the matrix board after the subsequent matrix board
 */
void MatrixBoardApplication::updateTraffic(unsigned int *traffic1, unsigned int *traffic2) {
    if (_subsequentBoardIDs.subsequent != 0) {
        if (_subsequentBoards.find(_subsequentBoardIDs.subsequent)->second->checkNewData()) {
            MBCData_Msg msgReceived = _subsequentBoards.find(_subsequentBoardIDs.subsequent)->second->getLastMessage();
            *traffic1 = static_cast<unsigned>(msgReceived.traffic);
            Logger::print(Logger::LogLevel::INFO, "Received from 1st with ID: ", msgReceived.userID,
                          " and traffic: ", msgReceived.traffic);
        }
    }
    if (_subsequentBoardIDs.afterSubsequent != 0) {
        if (_subsequentBoards.find(_subsequentBoardIDs.afterSubsequent)->second->checkNewData()) {
            MBCData_Msg msgReceived =
                _subsequentBoards.find(_subsequentBoardIDs.afterSubsequent)->second->getLastMessage();
            *traffic2 = static_cast<unsigned>(msgReceived.traffic);
            Logger::print(Logger::LogLevel::INFO, "Received from 2nd with ID: ", msgReceived.userID,
                          " and traffic: ", msgReceived.traffic);
        }
    }
}

/**
 * @brief calculateSpeed calculates the speed for on the matrix board
 *
 * @param traffic the amount of traffic that passed the sensor of the matrix board in front of it
 * @return unsigned int the speed that should be shown on the matrixboard
 */
unsigned int MatrixBoardApplication::calculateSpeed(const unsigned int traffic) {
    const unsigned int currentSpeed = display_.getDisplayValue();
    unsigned int newSpeed = currentSpeed;
    if (_oldTraffic < traffic) {
        if (currentSpeed > 50)
            newSpeed = currentSpeed - 10;
    } else if (_oldTraffic > traffic) {
        if (currentSpeed < 120)
            newSpeed = currentSpeed + 10;
    }
    _oldTraffic = traffic;
    return newSpeed;
}

/**
 * @brief Checks for a DDS error and throws an error upon failure
 *
 * @param value the returned value
 */
void MatrixBoardApplication::checkDDSerror(const int32_t value) {
    if (value < 0) {
        throw std::runtime_error(HelperFunctions::argToString("Matrixboard: ", __func__, ": ", dds_strretcode(-value)));
    }
}

/**
 * @brief Creates a MBCData_Msg with the wanted message
 *
 * @param message the message that is in the MBCData_Msg
 * @return MBCData_Msg a struct containing information that can be send using DDS
 */
MBCData_Msg MatrixBoardApplication::createMessage(const std::string &message) const {
    // Create a message and send it to its own topic
    MBCData_Msg msg;
    char messageChr[10];

    message.copy(messageChr, 10);

    msg.message = messageChr;
    msg.userID = static_cast<int32_t>(_id);
    return msg;
}

/**
 * @brief Removes the old matrix boards from the subsequent boards map
 *
 */
void MatrixBoardApplication::removeOldBoards() {
    std::vector<unsigned int> removableIDs;
    for (const auto &e : _subsequentBoards) {
        if (e.first != _subsequentBoardIDs.subsequent && e.first != _subsequentBoardIDs.afterSubsequent) {
            removableIDs.push_back(e.first);
        }
    }
    for (const auto &e : removableIDs) {
        _subsequentBoards.erase(e);
    }
}